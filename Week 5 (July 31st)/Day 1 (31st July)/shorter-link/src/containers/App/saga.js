import { takeLatest, call, put, select } from 'redux-saga/effects';

import { getShortUrl } from '@domain/api';
import { setShortUrl, setShortUrlLoading, setShortUrlError } from '@containers/App/actions';
import { GET_SHORT_URL } from '@containers/App/constants';
import { selectShortUrl } from './selectors';

export function* doGetShortUrl({ url }) {
  yield put(setShortUrlLoading(true));

  try {
    const array = yield select(selectShortUrl);

    if (url !== '' && array.map((item) => item.original_link).find((link) => link.includes(url))) {
      yield put(setShortUrlError({ errorCode: 11 }));

      yield put(setShortUrlLoading(false));

      return -1;
    }

    const shortUrl = yield call(getShortUrl, url);
    if (shortUrl?.result) {
      yield put(setShortUrlError(null));
      yield put(setShortUrl(shortUrl.result));
    }
  } catch (err) {
    const errorCode = err?.response?.data?.error_code;
    yield put(setShortUrlError({ errorCode }));
  }
  yield put(setShortUrlLoading(false));
}

export default function* appSaga() {
  yield takeLatest(GET_SHORT_URL, doGetShortUrl);
}
