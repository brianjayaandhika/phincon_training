import { useState, useRef } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import Loader from '@components/Loader';

// Redux
import PropTypes from 'prop-types';
import { connect, useDispatch } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { selectShortUrl, selectShortUrlLoading, selectShortUrlError } from '@containers/App/selectors';
import { getShortUrl, deleteShortUrl } from '@containers/App/actions';

// MUI
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material';
import FacebookIcon from '@mui/icons-material/Facebook';
import TwitterIcon from '@mui/icons-material/Twitter';
import PinterestIcon from '@mui/icons-material/Pinterest';
import InstagramIcon from '@mui/icons-material/Instagram';

// Images
import introImg from '@static/images/images/illustration-working.svg';
import graphImg from '@static/images/images/icon-brand-recognition.svg';
import recordsImg from '@static/images/images/icon-detailed-records.svg';
import customizeImg from '@static/images/images/icon-fully-customizable.svg';

import classes from './style.module.scss';

const Home = ({ shortUrl, shortUrlLoading, shortUrlError, intl }) => {
  // REVISI: ADAIN EROR GLOBAL

  const dispatch = useDispatch();
  const [cooldown, setCooldown] = useState(false);

  const cards = [
    {
      title: <FormattedMessage id="about_card_title1" />,
      subtitle: <FormattedMessage id="about_card_subtitle1" />,
      image: graphImg,
    },
    {
      title: <FormattedMessage id="about_card_title2" />,
      subtitle: <FormattedMessage id="about_card_subtitle2" />,
      image: recordsImg,
    },
    {
      title: <FormattedMessage id="about_card_title3" />,
      subtitle: <FormattedMessage id="about_card_subtitle3" />,
      image: customizeImg,
    },
  ];

  // Error message
  // const [errorMessage, setErrorMessage] = useState("");

  const handleError = () => {
    switch (shortUrlError.errorCode) {
      case 1:
        return intl.formatMessage({ id: 'error_1' });
      case 2:
        return intl.formatMessage({ id: 'error_2' });
      case 10:
        return intl.formatMessage({ id: 'error_10' });
      case 11:
        return intl.formatMessage({ id: 'error_11' });
      default:
        return intl.formatMessage({ id: 'error_default' });
    }
  };

  // INPUT HANDLE
  const [inputValue, setInputValue] = useState('');

  const inputRef = useRef(null);

  const handleSubmit = (e) => {
    e.preventDefault();

    dispatch(getShortUrl(inputValue));

    setInputValue('');

    if (inputRef.current) {
      inputRef.current.value = '';
    }

    setCooldown(true);

    setTimeout(() => {
      setCooldown(false);
    }, 2000);
  };

  const handleChange = (event) => {
    setInputValue(event.target.value);
  };

  // Handles for remove url Dialog
  const [open, setOpen] = useState(false);
  const [selectedCode, setSelectedCode] = useState('');

  const handleClickOpen = (code) => {
    setOpen(true);
    setSelectedCode(code);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleDelete = () => {
    dispatch(deleteShortUrl(selectedCode));
    setSelectedCode('');

    setOpen(false);
  };

  // Handle for Copy to clipboard
  const handleCopy = (url) => {
    navigator.clipboard.writeText(url.short_link);

    document.getElementById(`${url.code}`).innerHTML = intl.formatMessage({ id: 'input_copied' });
    document.getElementById(`${url.code}`).style.backgroundColor = 'hsl(257, 27%, 26%)';

    setTimeout(() => {
      document.getElementById(`${url.code}`).innerHTML = intl.formatMessage({ id: 'input_copy' });
      document.getElementById(`${url.code}`).style.backgroundColor = 'hsl(180, 66%, 49%)';
    }, 2500);
  };

  // Contents
  const renderContent = () => (
    <div className={classes.row}>
      <FormattedMessage id="input_empty" />
    </div>
  );

  return (
    <div className={classes.wrapper} id="home">
      {/* INTRO */}
      <div className={classes.intro}>
        <div className={classes.textGroup}>
          <h1 className={classes.title}>
            <FormattedMessage id="intro_title" />
          </h1>
          <p className={classes.subtitle}>
            <FormattedMessage id="intro_subtitle" />
          </p>
          <button className={classes.button} type="button">
            <FormattedMessage id="intro_button" />
          </button>
        </div>
        <img src={introImg} alt="stock" className={classes.img} />
      </div>

      {/* INPUT */}
      <div className={classes.inputWrapper} id="input">
        <form action="" className={classes.form} onSubmit={(e) => handleSubmit(e)} noValidate>
          <label htmlFor="input" className={classes.label}>
            <input
              ref={inputRef}
              className={classes.input}
              type="url"
              placeholder={intl.formatMessage({ id: 'input_placeholder' })}
              onChange={handleChange}
            />

            {shortUrlError && <div className={classes.error}>{handleError()}</div>}

            <button className={classes.button} type="submit" disabled={cooldown}>
              <FormattedMessage id="input_button" />
            </button>
          </label>
        </form>

        <div className={classes.shortenedWrapper}>
          {shortUrl.length > 0
            ? shortUrl?.map((url) => (
                <div className={classes.row} key={url.code}>
                  <p className={classes.link}>{url.original_link}</p>
                  <div className={classes.left}>
                    <a href={url.original_link} target="_blank" rel="noreferrer" className={classes.shortLink}>
                      {url.short_link}
                    </a>
                    <div className={classes.btnGroup}>
                      <button type="button" className={classes.button} onClick={() => handleCopy(url)} id={url.code}>
                        <FormattedMessage id="input_copy" />
                      </button>
                      <button type="button" className={classes.closeBtn} onClick={() => handleClickOpen(url.code)}>
                        <FormattedMessage id="input_remove" />
                      </button>
                    </div>
                  </div>
                </div>
              ))
            : renderContent()}
        </div>
      </div>

      {/* ABOUT */}
      <div className={classes.about} id="feature">
        <div className={classes.textWrapper}>
          <h1 className={classes.title}>
            <FormattedMessage id="about_title" />
          </h1>
          <p className={classes.subtitle}>
            <FormattedMessage id="about_subtitle" />
          </p>
        </div>
        <div className={classes.cardWrapper}>
          {cards.map((card, index) => (
            <div className={classes.card} key={index}>
              <div className={classes.imgWrapper}>
                <img src={card.image} alt="stock" style={{ color: 'red' }} />
              </div>
              <h1 className={classes.title}>{card.title}</h1>
              <p className={classes.subtitle}>{card.subtitle}</p>
            </div>
          ))}
          <div className={classes.line} />
          <div className={classes.lineMobile} />
        </div>
      </div>

      {/* START */}
      <div className={classes.start} id="start">
        <h1 className={classes.title}>
          {' '}
          <FormattedMessage id="start_title" />
        </h1>
        <button className={classes.button} type="button">
          <FormattedMessage id="start_button" />
        </button>
      </div>

      {/* FOOTER */}
      <footer className={classes.footer}>
        <div className={classes.logo}>Shortly</div>
        <ul className={classes.link}>
          <li className={classes.linkItem}>
            <h1 className={classes.title}>
              <FormattedMessage id="footer_title1" />
            </h1>
            <ul className={classes.listGroup}>
              <li className={classes.listItem}>
                <a href="/" className={classes.listLink}>
                  <FormattedMessage id="footer_title1_list1" />
                </a>
              </li>
              <li className={classes.listItem}>
                <a href="/" className={classes.listLink}>
                  <FormattedMessage id="footer_title1_list2" />
                </a>
              </li>
              <li className={classes.listItem}>
                <a href="/" className={classes.listLink}>
                  <FormattedMessage id="footer_title1_list3" />
                </a>
              </li>
            </ul>
          </li>
          <li className={classes.linkItem}>
            <h1 className={classes.title}>
              <FormattedMessage id="footer_title2" />
            </h1>
            <ul className={classes.listGroup}>
              <li className={classes.listItem}>
                <a href="/" className={classes.listLink}>
                  <FormattedMessage id="footer_title2_list1" />
                </a>
              </li>
              <li className={classes.listItem}>
                <a href="/" className={classes.listLink}>
                  <FormattedMessage id="footer_title2_list2" />
                </a>
              </li>
              <li className={classes.listItem}>
                <a href="/" className={classes.listLink}>
                  <FormattedMessage id="footer_title2_list3" />
                </a>
              </li>
            </ul>
          </li>
          <li className={classes.linkItem}>
            <h1 className={classes.title}>
              {' '}
              <FormattedMessage id="footer_title3" />
            </h1>
            <ul className={classes.listGroup}>
              <li className={classes.listItem}>
                <a href="/" className={classes.listLink}>
                  <FormattedMessage id="footer_title3_list1" />
                </a>
              </li>
              <li className={classes.listItem}>
                <a href="/" className={classes.listLink}>
                  <FormattedMessage id="footer_title3_list2" />
                </a>
              </li>
              <li className={classes.listItem}>
                <a href="/" className={classes.listLink}>
                  <FormattedMessage id="footer_title3_list3" />
                </a>
              </li>
              <li className={classes.listItem}>
                <a href="/" className={classes.listLink}>
                  <FormattedMessage id="footer_title3_list4" />
                </a>
              </li>
            </ul>
          </li>
        </ul>
        <div className={classes.icons}>
          <a href="/" className={classes.iconLink}>
            <FacebookIcon className={classes.icon} />
          </a>
          <a href="/" className={classes.iconLink}>
            <TwitterIcon className={classes.icon} />
          </a>
          <a href="/" className={classes.iconLink}>
            <PinterestIcon className={classes.icon} />
          </a>
          <a href="/" className={classes.iconLink}>
            <InstagramIcon className={classes.icon} />
          </a>
        </div>
      </footer>

      {/* DELETE DIALOG */}
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          <FormattedMessage id="dialog_title" />
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <FormattedMessage id="dialog_text" />
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>
            <FormattedMessage id="dialog_cancel" />
          </Button>
          <Button onClick={handleDelete} autoFocus color="error">
            <FormattedMessage id="dialog_delete" />
          </Button>
        </DialogActions>
      </Dialog>
      <Loader isLoading={shortUrlLoading} />
    </div>
  );
};

Home.propTypes = {
  shortUrl: PropTypes.array,
  shortUrlLoading: PropTypes.bool,
  shortUrlError: PropTypes.object,
  intl: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  shortUrl: selectShortUrl,
  shortUrlLoading: selectShortUrlLoading,
  shortUrlError: selectShortUrlError,
});

export default injectIntl(connect(mapStateToProps)(Home));
