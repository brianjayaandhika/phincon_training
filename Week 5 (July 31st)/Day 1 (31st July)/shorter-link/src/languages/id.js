export default {
  // Default
  app_title_header: 'Shortly',
  app_not_found: 'Halaman tidak ditemukan',
  app_lang_id: 'Bahasa Indonesia',
  app_lang_en: 'Bahasa Inggris',

  // Navbar
  nav_item1: 'Fitur',
  nav_item2: 'Harga',
  nav_item3: 'Sumber Daya',
  nav_item4: 'Bahasa',
  nav_login: 'Masuk',
  nav_signup: 'Daftar',

  // Intro
  intro_title: 'Lebih dari sekedar pemendek tautan',
  intro_subtitle: 'Bangun pengenalan merek Anda dan dapatkan wawasan terperinci tentang kinerja tautan anda.',
  intro_button: 'Mulai Sekarang',

  // Input
  input_placeholder: 'Pendekkan tautan disini...',
  input_button: 'Pendekkan!',
  input_copy: 'Salin',
  input_copied: 'Tersalin!',
  input_remove: 'Hapus',
  input_empty: 'Tidak ada tautan',

  // About
  about_title: 'Statistik Lanjutan',
  about_subtitle: 'Lacak kinerja tautan Anda di seluruh web dengan dasbor statistik lanjutan kami.',
  about_card_title1: 'Pengenalan Merek',
  about_card_title2: 'Catatan Detail',
  about_card_title3: 'Kustomisasi Sepenuhnya',
  about_card_subtitle1: 'Lacak kinerja tautan Anda di seluruh web dengan dasbor statistik lanjutan kami.',
  about_card_subtitle2:
    'Dapatkan wawasan tentang siapa yang mengklik tautan Anda. Mengetahui kapan dan di mana orang berinteraksi dengan konten Anda akan membantu Anda mengambil keputusan yang lebih baik.',
  about_card_subtitle3:
    'Tingkatkan kesadaran merek dan penemuan konten melalui tautan yang dapat disesuaikan, sehingga meningkatkan keterlibatan audiens.',

  // Start
  start_title: 'Tingkatkan tautan Anda hari ini',
  start_button: 'Mulai Sekarang',

  // Footer
  footer_title1: 'Fitur',
  footer_title2: 'Sumber Daya',
  footer_title3: 'Perusahaan',
  footer_title1_list1: 'Pemendek Tautan',
  footer_title1_list2: 'Tautan Bermerek',
  footer_title1_list3: 'Analisis',
  footer_title2_list1: 'Blog',
  footer_title2_list2: 'Pengembang',
  footer_title2_list3: 'Dukungan',
  footer_title3_list1: 'Tentang',
  footer_title3_list2: 'Tim Kami',
  footer_title3_list3: 'Karir',
  footer_title3_list4: 'Kontak',

  // Dialog
  dialog_title: 'Hapus tautan ini?',
  dialog_text: 'Jika Anda menghapus tautan ini, tidak akan ada cadangan dan Anda harus memendekkannya kembali.',
  dialog_cancel: 'Batal',
  dialog_delete: 'Hapus',

  // Error Message
  error_1: 'Tambahkan Tautan',
  error_2: 'Tautan Tidak Valid',
  error_10: 'Tautan Tidak Diizinkan',
  error_11: 'Tautan Duplikat Terdeteksi!',
  error_default: 'Ada sesuatu yang salah...',
};
