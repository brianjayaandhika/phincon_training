export default {
  // Default
  app_title_header: 'Vite + React',
  app_not_found: 'Page not found',
  app_lang_id: 'Indonesian',
  app_lang_en: 'English',

  // Navbar
  nav_item1: 'Features',
  nav_item2: 'Pricing',
  nav_item3: 'Resources',
  nav_item4: 'Language',
  nav_login: 'Login',
  nav_signup: 'Sign Up',

  // Intro
  intro_title: 'More than just shorter links',
  intro_subtitle: 'Build your brand`s recognition and get detailed insights on how your links are performing.',
  intro_button: 'Get Started',

  // Input
  input_placeholder: 'Shorten a link here...',
  input_button: 'Shorten It!',
  input_copy: 'Copy',
  input_copied: 'Copied!',
  input_remove: 'Remove',
  input_empty: 'No Link Available',

  // About
  about_title: 'Advanced Statistics',
  about_subtitle: 'Track how your links are performing across the web with our advanced statistics dashboard.',
  about_card_title1: 'Brand Recognition',
  about_card_title2: 'Detailed Records',
  about_card_title3: 'Fully Customizable',
  about_card_subtitle1: 'Track how your links are performing across the web with our advanced statistics dashboard.',
  about_card_subtitle2:
    'Gain insights into who is clicking your links. Knowing when and where people engage with your content helps inform better decisions.',
  about_card_subtitle3:
    'Improve brand awareness and content discoverability through customizable links, supercharging audience engagement.',

  // Start
  start_title: 'Boost your links today',
  start_button: 'Get Started',

  // Footer
  footer_title1: 'Features',
  footer_title2: 'Resources',
  footer_title3: 'Company',
  footer_title1_list1: 'Link Shortening',
  footer_title1_list2: 'Branded Links',
  footer_title1_list3: 'Analytics',
  footer_title2_list1: 'Blog',
  footer_title2_list2: 'Developers',
  footer_title2_list3: 'Support',
  footer_title3_list1: 'About',
  footer_title3_list2: 'Our Team',
  footer_title3_list3: 'Careers',
  footer_title3_list4: 'Contact',

  // Dialog
  dialog_title: 'Delete this URL?',
  dialog_text: 'If you delete this URL, there will be no back up and you have to shorten it again.',
  dialog_cancel: 'Cancel',
  dialog_delete: 'Delete',

  // Error Message
  error_1: 'Please add a URL',
  error_2: 'Invalid URL',
  error_10: 'Disallowed URL',
  error_11: 'Duplicate URL Detected!',
  error_default: 'Something went wrong...',
};
