import PropTypes from 'prop-types';
import { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import AppBar from '@mui/material/AppBar';
import Avatar from '@mui/material/Avatar';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';

import { setLocale } from '@containers/App/actions';

import FlagId from '@static/images/flags/id.png';
import FlagEn from '@static/images/flags/en.png';

import classes from './style.module.scss';

const Navbar = ({ locale, contentRef }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [menuPosition, setMenuPosition] = useState(null);
  const open = Boolean(menuPosition);
  const [checked, setChecked] = useState(false);

  const handleChecked = () => {
    setChecked((prevState) => !prevState);
  };

  const handleClick = (event) => {
    setMenuPosition(event.currentTarget);
  };

  const handleClose = () => {
    setMenuPosition(null);
  };

  const onSelectLang = (lang) => {
    if (lang !== locale) {
      dispatch(setLocale(lang));
    }
    handleClose();
  };

  const goHome = () => {
    navigate('/');
  };

  return (
    <AppBar className={classes.headerWrapper} ref={contentRef}>
      <div className={classes.contentWrapper}>
        <div className={classes.logoImage} onClick={goHome}>
          <div className={classes.title}>Shortly</div>
        </div>
        {/* Desktop */}
        <div className={classes.navbarDesktop}>
          <ul className={classes.link}>
            <li className={classes.linkItem}>
              <a href="#input">
                <FormattedMessage id="nav_item1" />
              </a>
            </li>
            <li className={classes.linkItem}>
              <a href="#start">
                <FormattedMessage id="nav_item2" />
              </a>
            </li>
            <li className={classes.linkItem}>
              <a href="/">
                <FormattedMessage id="nav_item3" />
              </a>
            </li>
          </ul>
          <div className={classes.button}>
            <div className={classes.toolbar}>
              <div className={classes.toggle} onClick={handleClick}>
                <Avatar className={classes.avatar} src={locale === 'id' ? FlagId : FlagEn} />
              </div>
            </div>
            <button className={classes.login} type="button">
              <FormattedMessage id="nav_login" />
            </button>
            <button className={classes.signUp} type="button">
              <FormattedMessage id="nav_signup" />
            </button>
          </div>
        </div>

        {/* Mobile */}
        <section className={classes.topNav}>
          {/* eslint-disable */}
          <input id={classes.menuToggle} type="checkbox" checked={checked} onChange={() => handleChecked()} />
          <label className={classes.menuButtonContainer} htmlFor={classes.menuToggle}>
            {/* eslint-enable */}
            <div className={classes.menuButton} />
          </label>
          <ul className={classes.menu}>
            <li className={classes.linkItem}>
              <a href="#about" onClick={() => handleChecked()}>
                <FormattedMessage id="nav_item1" />
              </a>
            </li>
            <li className={classes.linkItem}>
              <a href="#start" onClick={() => handleChecked()}>
                <FormattedMessage id="nav_item2" />
              </a>
            </li>
            <li className={classes.linkItem}>
              <a href="/" onClick={() => handleChecked()}>
                <FormattedMessage id="nav_item3" />
              </a>
            </li>
            <li className={classes.toolbar}>
              <div className={classes.toggle} onClick={handleClick}>
                <span className={classes.language}>
                  <FormattedMessage id="nav_item4" />
                </span>
                <Avatar className={classes.avatar} src={locale === 'id' ? FlagId : FlagEn} />
              </div>
            </li>
            <li>
              <a href="/" onClick={() => handleChecked()}>
                <FormattedMessage id="nav_login" />
              </a>
            </li>
            <li>
              <button className={classes.signUp} type="button" onClick={() => handleChecked()}>
                <FormattedMessage id="nav_signup" />
              </button>
            </li>
          </ul>
          <div className={classes.staticBg} />
        </section>

        <Menu open={open} anchorEl={menuPosition} onClose={handleClose} onClick={() => handleChecked()}>
          <MenuItem onClick={() => onSelectLang('id')} selected={locale === 'id'}>
            <div className={classes.menu}>
              <Avatar className={classes.menuAvatar} src={FlagId} />
              <div className={classes.menuLang}>
                <FormattedMessage id="app_lang_id" />
              </div>
            </div>
          </MenuItem>
          <MenuItem onClick={() => onSelectLang('en')} selected={locale === 'en'}>
            <div className={classes.menu}>
              <Avatar className={classes.menuAvatar} src={FlagEn} />
              <div className={classes.menuLang}>
                <FormattedMessage id="app_lang_en" />
              </div>
            </div>
          </MenuItem>
        </Menu>
      </div>
    </AppBar>
  );
};

Navbar.propTypes = {
  locale: PropTypes.string.isRequired,
  contentRef: PropTypes.object,
};

export default Navbar;
