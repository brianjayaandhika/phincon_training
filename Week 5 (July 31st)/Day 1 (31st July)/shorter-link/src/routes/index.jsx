import Home from '@pages/Home';
import NotFound from '@pages/NotFound';

import MainLayout from '@layouts/MainLayout';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    layout: MainLayout,
  },

  { path: '*', name: 'Not Found', component: NotFound },
];

export default routes;
