import axios from "axios";

// JAWABAN DARI MAS NABHAN, JAUH LEBIH RAPIH
// https://gitlab.com/nabhannaufal/anime-countdown/-/tree/main/

// 1. Timer
const timer = (time) => {
  let countdown = time;

  let interval = setInterval(function () {
    let result = new Date(countdown * 1000).toISOString().slice(11, 19);
    console.log(result);
    countdown--;

    if (countdown < 0) {
      clearInterval(interval);
    }
  }, 1000);
};

// 2.1 Anime List --------------------------------------
const getAnimeList = async () => {
  await axios({
    method: "get",
    url: "https://api.jikan.moe/v4/recommendations/anime",
  })
    .then((response) => {
      const data = response.data.data;

      const dataList = data.map((e) => {
        return e.entry.map((e) => {
          return e.title;
        });
      });

      const mergedList = dataList.flat().slice(0, 20);

      return mergedList;
    })
    .then((response) => {
      console.log(response);
    });
};

// 2.2 Sorted List --------------------------------------
const getSortedAnimeList = async () => {
  await axios({
    method: "get",
    url: "https://api.jikan.moe/v4/recommendations/anime",
  })
    .then((response) => {
      const data = response.data.data;

      const dataList = data.map((e) => {
        return e.entry
          .map((e) => {
            return e.title;
          })
          .sort((a, b) => a.dateList - b.dateList);
      });

      return dataList.flat().slice(0, 20);
    })
    .then((response) => {
      console.log(response);
    });
};

// 2.3 5 most popular anime --------------------------------------
const getPopularAnime = async () => {
  await axios({
    method: "get",
    url: "https://api.jikan.moe/v4/recommendations/anime",
  }).then((response) => {
    const data = response.data.data;
    const slicedData = data.slice(0, 5);

    // Get ID utk mendapatkan detail anime
    const idList = slicedData.map((e) => {
      return e.entry.map((e) => {
        return e.mal_id;
      });
    });

    const flattedIdList = idList.flat();
    let fiveMostPopular = [...new Set([...flattedIdList])];

    let animeArray = [];

    for (let i = 0; i < fiveMostPopular.length; i += 1) {
      setTimeout(() => {
        for (let j = i; j < i + 1 && j < fiveMostPopular.length; j++) {
          axios({
            method: "get",
            url: `https://api.jikan.moe/v4/anime/${fiveMostPopular[j]}`,
          })
            .then((response) => {
              console.log("Fetching Data...");
              animeArray.push([response.data.data.title, response.data.data.popularity]);
            })
            .catch((err) => {
              console.error(err);
            });
        }
      }, (i / 1) * 1500);
    }

    setTimeout(() => {
      let sortedAnime = animeArray.sort((a, b) => a[1] - b[1]);
      let titleArr = sortedAnime.map((e) => {
        return `${e[0]}, popularity: ${e[1]}`;
      });

      console.log(titleArr.slice(0, 5));
    }, (fiveMostPopular.length / 1) * 1500);
  });
};

// 2.4 5 top rank anime --------------------------------------
const getTopRankAnime = async () => {
  await axios({
    method: "get",
    url: "https://api.jikan.moe/v4/recommendations/anime",
  }).then((response) => {
    const data = response.data.data;
    const slicedData = data.slice(0, 5);

    // Get ID utk mendapatkan detail anime
    const idList = slicedData.map((e) => {
      return e.entry.map((e) => {
        return e.mal_id;
      });
    });

    const flattedIdList = idList.flat();
    let fiveTopRank = [...new Set([...flattedIdList])];

    let animeArray = [];

    for (let i = 0; i < fiveTopRank.length; i += 1) {
      setTimeout(() => {
        for (let j = i; j < i + 1 && j < fiveTopRank.length; j++) {
          axios({
            method: "get",
            url: `https://api.jikan.moe/v4/anime/${fiveTopRank[j]}`,
          })
            .then((response) => {
              console.log("Fetching Data...");
              animeArray.push([response.data.data.title, response.data.data.rank]);
            })
            .catch((err) => {
              console.error(err);
            });
        }
      }, (i / 1) * 1500);
    }

    setTimeout(() => {
      let sortedAnime = animeArray.sort((a, b) => a[1] - b[1]);
      let titleArr = sortedAnime.map((e) => {
        return `${e[0]}, rank: ${e[1]}`;
      });

      console.log(titleArr.slice(0, 5));
    }, (fiveTopRank.length / 1) * 1500);
  });
};

// 2.5 most episode anime --------------------------------------
const getMostEpisode = async () => {
  await axios({
    method: "get",
    url: "https://api.jikan.moe/v4/recommendations/anime",
  }).then((response) => {
    const data = response.data.data;
    const slicedData = data.slice(0, 10);

    // Get ID utk mendapatkan detail anime
    const idList = slicedData.map((e) => {
      return e.entry.map((e) => {
        return e.mal_id;
      });
    });

    const flattedIdList = idList.flat();
    let mostEpisodeList = [...new Set([...flattedIdList])];

    let animeArray = [];

    for (let i = 0; i < mostEpisodeList.length; i += 1) {
      setTimeout(() => {
        for (let j = i; j < i + 1 && j < mostEpisodeList.length; j++) {
          axios({
            method: "get",
            url: `https://api.jikan.moe/v4/anime/${mostEpisodeList[j]}`,
          })
            .then((response) => {
              console.log("Fetching Data...");
              animeArray.push([response.data.data.title, response.data.data.episodes]);
            })
            .catch((err) => {
              console.error(err);
            });
        }
      }, (i / 1) * 1500);
    }

    setTimeout(() => {
      let sortedAnime = animeArray.sort((a, b) => b[1] - a[1]);
      let titleArr = sortedAnime.map((e) => {
        return `${e[0]}, episode: ${e[1]}`;
      });

      console.table(titleArr);
    }, (mostEpisodeList.length / 1) * 1500);
  });
};

// timer(3600); // 1
// getAnimeList(); // 2.1
// getSortedAnimeList(); // 2.2
// getPopularAnime(); // 2.3
// getTopRankAnime(); // 2.4
// getMostEpisode(); // 2.5
