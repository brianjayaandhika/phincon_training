// Alternatif
// https://gitlab.com/azmizein/list/-/blob/day4/day4_KalAndBalance.js

// 1. Operation Function
const operation = (a, b, oper) => {
  let result = 0;

  switch (oper) {
    case "add":
      result += a + b;
      break;
    case "subtract":
      result += a - b;
      break;
    case "divide":
      result += a / b;
      break;
    case "multiply":
      result += a * b;
      break;
    default:
      return "Operation not detected!";
  }

  if (result == "Infinity") {
    return undefined;
  }

  let finalRes = Math.floor(result);
  return Math.floor(finalRes);
};

// console.log(operation(2, 3, "add"));
// console.log(operation(1, 5, "subtract"));
// console.log(operation(6, 4, "divide"));
// console.log(operation(2, 5, "multiply"));
// console.log(operation(2, 0, "divide"));
// console.log(operation(5, 0, "anything"));

// --------------------------------------------------------------------------------------------
// 2. Return only element with numbers and letter on an array
const numInStr = (arr) => {
  let newArr = [];
  let numbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];

  for (let i = 0; i < arr.length; i++) {
    for (let j = 0; j < numbers.length; j++) {
      if (arr[i].includes(numbers[j])) {
        newArr.push(arr[i]);
        break;
      }
    }
  }

  return newArr;
};

// console.log(numInStr(["1a", "a", "2b", "b"]));
// console.log(numInStr(["abc", "abc10"]));
// console.log(numInStr(["abc", "ab10c", "a10bc", "bcd"]));
// console.log(numInStr(["rct", "ABC", "Test", "xYz"]));
// console.log(numInStr(["this IS", "10xYZ", "xy2K77", "Z1K2W0", "xYz"]));

// --------------------------------------------------------------------------------------------
// 3. Balancing the letters of a word
const balance = (string) => {
  const alphabet = "abcdefghijklmnopqrstuvwxyz";
  const alphabetIndex = alphabet.split("");
  let multiplier = 1;
  let result = 0;
  //   PAKE INDEX OF BUAT DAPET NILAI ALPHABET

  for (let i = 0; i < string.length; i++) {
    if (string.length % 2 === 0 && i === string.length / 2) {
      multiplier = -1;
    } else if (string.length % 2 !== 0) {
      if (i === Math.floor(string.length / 2)) {
        multiplier = 0;
      } else if (i > Math.floor(string.length / 2)) {
        multiplier = -1;
      }
    }

    result += multiplier * (alphabetIndex.indexOf(string[i]) + 1);
  }

  return `${string}, ${result === 0 ? true : false}`;
};

// console.log(balance("zips"));
// console.log(balance("brake"));
// console.log(balance("at"));
// console.log(balance("forgetful"));
// console.log(balance("vegetation"));
// console.log(balance("disillusioned"));
// console.log(balance("abstract"));
// console.log(balance("clever"));
// console.log(balance("conditionalities"));
// console.log(balance("seasoning"));

// --------------------------------------------------------------------------------------------
// 4. 2048 ??
const leftSlide = (arr) => {
  let sortedArr = arr.sort((a, b) => {
    if (b === 0) return -1; // Tuker Posisi
  });

  for (let i = 0; i < sortedArr.length; i++) {
    if (sortedArr[i] === sortedArr[i + 1]) {
      sortedArr[i] += sortedArr[i + 1];
      sortedArr[i + 1] = 0;
    }
  }

  let sortedSortedArr = sortedArr.sort((a, b) => {
    if (b === 0) return -1; // Tuker Posisi
  });

  return sortedSortedArr;
};

console.log(leftSlide([2, 2, 2, 0]));
console.log(leftSlide([2, 2, 4, 4, 8, 8]));
console.log(leftSlide([0, 2, 0, 2, 4]));
console.log(leftSlide([0, 2, 2, 8, 8, 8]));
console.log(leftSlide([0, 0, 0, 0]));
console.log(leftSlide([0, 0, 0, 2]));
console.log(leftSlide([2, 0, 0, 0]));
console.log(leftSlide([8, 2, 2, 4]));
console.log(leftSlide([1024, 1024, 1024, 512, 512, 256, 256, 128, 128, 64, 32, 32]));
