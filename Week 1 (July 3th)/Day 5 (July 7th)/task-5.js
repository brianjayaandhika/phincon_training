// Exercise Day 5

// 1. Create a function that changes specific words into emoticons. Given a sentence as a string, replace the words smile, grin, sad and mad with their corresponding emoticons

const emotify = (str) => {
  const emoticon = {
    smile: ":D",
    grin: ":)",
    sad: ":(",
    mad: ":P",
  };
  const word = str.split(" ");

  console.log(`${word[0]} ${word[1]} ${emoticon[word[2]]}`);
  return `${word[0]} ${word[1]} ${emoticon[word[2]]}`;
};

// emotify("Make me smile");
// emotify("Make me grin");
// emotify("Make me sad");
// emotify("Make me mad");

// 2. Create a function that takes a string as an argument and converts the first character of each word to uppercase. Return the newly formatted string.

const makeTitle = (str) => {
  const arrStr = str.split(" ");
  const newStr = arrStr.map((e) => {
    let capitalizedStr = e.charAt(0).toUpperCase() + e.substring(1, e.length);

    return capitalizedStr;
  });

  console.log(newStr.join(" "));
};

// makeTitle("This is a title");
// makeTitle("capitalize every word");
// makeTitle("I Like Pizza");
// makeTitle("PIZZA PIZZA PIZZA");
// makeTitle("I am a title");
// makeTitle("I AM A TITLE");

// 3. Create a function that takes an array of numbers between 1 and 10 (excluding one number) and returns the missing number.

const missingNum = (arr) => {
  const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  const total = numbers.reduce((acc, cur) => acc + cur, 0);
  console.log(arr.reduce((acc, cur) => acc - cur, total));
};

// missingNum([1, 2, 3, 4, 6, 7, 8, 9, 10]);
// missingNum([7, 2, 3, 6, 5, 9, 1, 4, 8]);
// missingNum([10, 5, 1, 2, 4, 6, 8, 3, 9]);
// missingNum([1, 2, 3, 4, 6, 7, 8, 9, 10]);
// missingNum([7, 2, 3, 6, 5, 9, 1, 4, 8]);
// missingNum([7, 2, 3, 9, 4, 5, 6, 8, 10]);

// 4. Create a function that takes an array of items, removes all duplicate items and returns a new array in the same sequential order as the old array (minus duplicates).

const removeDups = (arr) => {
  console.log([...new Set([...arr])]);
};

// removeDups([1, 0, 1, 0]);
// removeDups(["The", "big", "cat"]);
// removeDups(["John", "Taylor", "John"]);
// removeDups(["John", "Taylor", "John", "john"]);
// removeDups(["javascript", "python", "python", "ruby", "javascript", "c", "ruby"]);
// removeDups([1, 2, 2, 2, 3, 2, 5, 2, 6, 6, 3, 7, 1, 2, 5]);
// removeDups(["#", "#", "%", "&", "#", "$", "&"]);
// removeDups([3, "Apple", 3, "Orange", "Apple"]);

// 5. Create the function that takes an array with objects and returns the sum of people's budgets.

const getBudgets = (data) => {
  const totalBudget = data.map((e) => {
    return e.budget;
  });

  console.log(totalBudget.reduce((acc, cur) => acc + cur, 0));
};

// getBudgets([
//   { name: "John", age: 21, budget: 23000 },
//   { name: "Steve", age: 32, budget: 40000 },
//   { name: "Martin", age: 16, budget: 2700 },
// ]);
// getBudgets([
//   { name: "John", age: 21, budget: 29000 },
//   { name: "Steve", age: 32, budget: 32000 },
//   { name: "Martin", age: 16, budget: 1600 },
// ]);
// getBudgets([
//   { name: "John", age: 21, budget: 19401 },
//   { name: "Steve", age: 32, budget: 12321 },
//   { name: "Martin", age: 16, budget: 1204 },
// ]);
// getBudgets([
//   { name: "John", age: 21, budget: 10234 },
//   { name: "Steve", age: 32, budget: 21754 },
//   { name: "Martin", age: 16, budget: 4935 },
// ]);

// 6. An array is special if every even index contains an even number and every odd index contains an odd number. Create a function that returns true if an array is special, and false otherwise.

const isSpecialArray = (arr) => {
  for (let i = 0; i < arr.length; i++) {
    if (i % 2 === 0) {
      if (arr[i] % 2 === 0) {
        continue;
      } else if (arr[i] % 2 !== 0) {
        return false;
      }
    }

    if (i % 2 !== 0) {
      if (arr[i] % 2 !== 0) {
        continue;
      } else if (arr[i] % 2 === 0) {
        return false;
      }
    }

    if (i % 2 !== 1 && arr[i] % 2 === 0) {
      return false;
    }
  }

  return true;
};

// console.log(isSpecialArray([2, 7, 4, 9, 6, 1, 6, 3]));
// console.log(isSpecialArray([2, 7, 9, 1, 6, 1, 6, 3]));
// console.log(isSpecialArray([2, 7, 8, 8, 6, 1, 6, 3]));
// console.log(isSpecialArray([1, 1, 1, 2]));
// console.log(isSpecialArray([2, 2, 2, 2]));
// console.log(isSpecialArray([2, 1, 2, 1]));
// console.log(isSpecialArray([4, 5, 6, 7]));
// console.log(isSpecialArray([4, 5, 6, 7, 0, 5]));

// 7. Given an input string, reverse the string word by word, the first word will be the last, and so on.

const reverseWords = (str) => {
  const trimmedStr = str.trim();

  const reversedStr = trimmedStr.split(" ").reverse().join(" ");

  console.log(reversedStr);
};

// reverseWords(" the sky is blue");
// reverseWords("hello   world!  "); // Test Case masih gagal.
// reverseWords("a good example");
// reverseWords("hello world!");
// reverseWords("blue is sky the");
// reverseWords("a good example");
// reverseWords("fraud! of example another is this");
// reverseWords("man! the are You");

// 8. Create a function that returns true if an asterisk * is inside a box.
// Notes
// The asterisk may be in the array, however, it must be inside the box, if it exists.

// Dari mas nabhan

const inBox = (arr) => {
  let result = false;
  arr.slice(1, -1).forEach((item) => {
    if (/#\s*\*\s*#/.test(item)) result = true;
  });
  console.log(result);
  return result;
};

// inBox(["###", "#*#", "###"]);

// inBox(["####", "#* #", "#  #", "####"]);

// inBox(["*####", "# #", "#  #*", "####"]);

// inBox(["#####", "#   #", "#   #", "#   #", "#####"]);

// inBox(["###", "# #", "###"]);

// inBox(["####", "#  #", "#  #", "####"]);

// inBox(["#####", "#   #", "#   #", "#   #", "#####"]);

// inBox(["###", "#*#", "###"]);

// inBox(["####", "# *#", "#  #", "####"]);

// inBox(["#####", "#  *#", "#   #", "#   #", "#####"]);

// inBox(["#####", "#   #", "# * #", "#   #", "#####"]);

// inBox(["#####", "#   #", "#   #", "# * #", "#####"]);

// inBox(["#####", "#*  #", "#   #", "#   #", "#####"]);

// 9. You are given a dictionary of names and the amount of points they have. Return a dictionary with the same names, but instead of points, return what prize they get.
// "Gold", "Silver", or "Bronze" to the 1st, 2nd and 3rd place respectively. For all the other names, return "Participation" for the prize.

const awardPrizes = (object) => {
  const scores = Object.values(object).sort((a, b) => b - a);
  const toArr = Object.entries(object);
  const newArr = [];

  for (let i = 0; i < toArr.length; i++) {
    if (toArr[i][1] === scores[0]) {
      newArr.push([toArr[i][0], "Gold"]);
    } else if (toArr[i][1] === scores[1]) {
      newArr.push([toArr[i][0], "Silver"]);
    } else if (toArr[i][1] === scores[2]) {
      newArr.push([toArr[i][0], "Bronze"]);
    } else {
      newArr.push([toArr[i][0], "Participation"]);
    }
  }

  console.log(Object.fromEntries(newArr));
};

// awardPrizes({
//   Joshua: 45,
//   Alex: 39,
//   Eric: 43,
// });

// awardPrizes({
//   "Person A": 1,
//   "Person B": 2,
//   "Person C": 3,
//   "Person D": 4,
//   "Person E": 102,
// });

// awardPrizes({
//   Mario: 99,
//   Luigi: 100,
//   Yoshi: 299,
//   Toad: 2,
// });

// 10. Create a function that determines how many number pairs are embedded in a space-separated string. The first numeric value in the space-separated string represents the count of the numbers, thus, excluded in the pairings.
// Notes
// Always take into consideration the first number in the string is not part of the pairing, thus, the count. It may not seem so useful as most people see it, but it's mathematically significant if you deal with set operations.

const number_pairs = (string) => {
  const checkArr = [];
  const strToArray = string.split(" ");
  let counter = 0;

  for (let i = 1; i < strToArray.length; i++) {
    if (!checkArr.includes(strToArray[i])) {
      checkArr.push(strToArray[i]);
    } else {
      counter++;
      checkArr[checkArr.indexOf(strToArray[i])] = undefined;
    }
  }

  console.log(counter);
};

// number_pairs("7 1 2 1 2 1 3 2");
// number_pairs("9 10 20 20 10 10 30 50 10 20");
// number_pairs("4 2 3 4 1");

// 11. Abigail and Benson are playing Rock, Paper, Scissors.
// Each game is represented by an array of length 2, where the first element represents what Abigail played and the second element represents what Benson played.

const calculateScore = (array) => {
  // R > S > P > R

  let score = 0;
  // 1 = Abigail, 0 = tie, -1 = benson

  array.forEach((e) => {
    if (e[0] === "R") {
      e[1] === "R" ? (score += 0) : e[1] === "S" ? (score += 1) : (score -= 1);
    } else if (e[0] === "P") {
      e[1] === "P" ? (score += 0) : e[1] === "R" ? (score += 1) : (score -= 1);
    } else if (e[0] === "S") {
      e[1] === "S" ? (score += 0) : e[1] === "P" ? (score += 1) : (score -= 1);
    }
  });

  score === 0
    ? console.log("Tie")
    : score >= 1
    ? console.log("Abigail")
    : score >= -1
    ? console.log("Benson")
    : "Failed";
};

// calculateScore([
//   ["R", "R"],
//   ["S", "S"],
// ]);
// calculateScore([
//   ["S", "R"],
//   ["R", "S"],
//   ["R", "R"],
// ]);
// calculateScore([
//   ["R", "P"],
//   ["R", "S"],
//   ["S", "P"],
// ]);
// calculateScore([
//   ["R", "R"],
//   ["S", "S"],
// ]);
// calculateScore([
//   ["S", "R"],
//   ["R", "S"],
//   ["R", "R"],
// ]);
// calculateScore([
//   ["S", "R"],
//   ["P", "R"],
// ]);
// calculateScore([
//   ["S", "S"],
//   ["S", "P"],
//   ["R", "S"],
//   ["S", "R"],
//   ["R", "R"],
// ]);
// calculateScore([
//   ["S", "R"],
//   ["S", "R"],
//   ["S", "R"],
//   ["R", "S"],
//   ["R", "S"],
// ]);

// 12. Given a matrix of m * n elements (m rows, n columns) ➞ return all elements of the matrix in spiral order.

const spiralOrder = (arr) => {
  const spiralArr = [
    ...arr[0],
    arr[1][arr[1].length - 1],
    ...arr[2].reverse(),
    ...arr[1],
  ];
  const setSpiral = [...new Set([...spiralArr])];

  console.log(setSpiral);
};

// spiralOrder([
//   [1, 2, 3],
//   [4, 5, 6],
//   [7, 8, 9],
// ]);

// spiralOrder([
//   [1, 2, 3, 4],
//   [5, 6, 7, 8],
//   [9, 10, 11, 12],
// ]);

// spiralOrder([
//   [1, 2, 3],
//   [4, 5, 6],
//   [7, 8, 9],
// ]);

// spiralOrder([
//   [1, 2, 3, 4],
//   [5, 6, 7, 8],
//   [9, 10, 11, 12],
// ]);

// spiralOrder([
//   [1, 2, 3, 4, 5],
//   [6, 7, 8, 9, 10],
//   [11, 12, 13, 14, 15],
// ]);

// spiralOrder([
//   [1, 2, 3, 4, 5, 6],
//   [7, 8, 9, 10, 11, 12],
//   [13, 14, 15, 16, 17, 18],
// ]);

// spiralOrder([
//   [13, 14, 15, 16, 17, 18],
//   [1, 2, 3, 4, 5, 6],
//   [7, 8, 9, 10, 11, 12],
// ]);

// spiralOrder([
//   [13, 14, 15, 16, 17, 18, 19, 20, 21, 22],
//   [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
//   [7, 8, 9, 10, 11, 12, 23, 24, 25, 26],
// ]);

// 13. A countdown sequence is a descending sequence of k integers from k down to 1 (e.g. 5, 4, 3, 2, 1). Write a function that returns an array of the form[x, y] where x is the number of countdown sequences in the given array and y is the array of those sequences in the order in which they appear in the array.

const finalCountdown = (arr) => {
  // dari mas nabhan

  let count = 0;
  const sequenceArray = [];
  const countData = arr.reverse();

  const collectSequence = (arr) => {
    const sequence = [];
    for (let i = 0; i < arr.length; i++) {
      sequence.unshift(arr[i]);
      if (arr[i] !== arr[i + 1] - 1) break;
    }
    sequenceArray.unshift(sequence);
  };

  for (let i = 0; i < countData.length; i++) {
    if (countData[i] === 1) {
      collectSequence(countData.slice(i));
      count++;
    }
  }
  console.log([count, sequenceArray]);
  return [count, sequenceArray];

  //   console.log(result.length, result);
};

// finalCountdown([1, 1, 2, 1]);
// finalCountdown([5, 4, 3, 2, 1]);
// finalCountdown([2, 5, 4, 3, 2, 1, 2]);
// finalCountdown([2, 3, 2, 1, 4, 3, 2, 1]);
// finalCountdown([4, 3, 2, 5, 4, 3]);
// finalCountdown([4, 3, 2, 5, 4, 3, 1]);
// finalCountdown([3, 2, 1, 5, 5, 3, 2, 1, 5, 5]);
// finalCountdown([4, 8, 3, 2, 1, 2]);
// finalCountdown([4, 4, 5, 4, 3, 2, 1, 8, 3, 2, 1]);
// finalCountdown([4, 3, 2, 1, 3, 2, 1, 1]);
// finalCountdown([1, 2, 1, 1]);
// finalCountdown([1, 2, 3, 4, 3, 2, 1]);
// finalCountdown([]);
// finalCountdown([2, 1, 2, 1]);

// 14. Create a function that takes a Tic-tac-toe board and returns "X" if the X's are placed in a way that there are three X's in a row or returns "O" if there are three O's in a row.
// Notes:
// All places on the board will have either "X" or "O". If both "X" and "O" win, return "Tie".

// dari mas nabhan

const whoWon = (board) => {
  const r1 = board[0];
  const r2 = board[1];
  const r3 = board[2];
  const win = [];

  if (r1[0] === r1[1] && r1[0] === r1[2]) win.push(r1[0]);
  if (r2[0] === r2[1] && r2[0] === r2[2]) win.push(r2[0]);
  if (r3[0] === r3[1] && r3[0] === r3[2]) win.push(r3[0]);
  if (r1[0] === r2[0] && r1[0] === r3[0]) win.push(r1[0]);
  if (r1[1] === r2[1] && r1[1] === r3[1]) win.push(r1[1]);
  if (r1[2] === r2[2] && r1[2] === r3[2]) win.push(r1[2]);
  if (r1[0] === r2[1] && r1[0] === r3[2]) win.push(r1[0]);
  if (r1[2] === r2[1] && r1[2] === r3[0]) win.push(r1[2]);
  if ((win.includes("X") && win.includes("O")) || win.length < 1) return "Tie";
  console.log(win[0]);
  return win[0];

  // cek vertikal
};

// whoWon([
//   ["O", "X", "O"],
//   ["X", "X", "O"],
//   ["O", "X", "X"],
// ]);

// whoWon([
//   ["O", "O", "X"],
//   ["X", "O", "X"],
//   ["O", "X", "O"],
// ]);

// whoWon([
//   ["O", "O", "X"],
//   ["X", "X", "O"],
//   ["O", "X", "O"],
// ]);

// whoWon([
//   ["X", "O", "X"],
//   ["X", "O", "O"],
//   ["X", "X", "O"],
// ]);

// whoWon([
//   ["O", "X", "O"],
//   ["X", "X", "O"],
//   ["O", "X", "X"],
// ]);

// whoWon([
//   ["X", "X", "O"],
//   ["O", "X", "O"],
//   ["X", "O", "O"],
// ]);

// whoWon([
//   ["X", "X", "X"],
//   ["O", "X", "O"],
//   ["X", "O", "O"],
// ]);

// whoWon([
//   ["X", "O", "X"],
//   ["O", "O", "O"],
//   ["X", "X", "O"],
// ]);

// whoWon([
//   ["O", "O", "X"],
//   ["X", "O", "X"],
//   ["O", "X", "O"],
// ]);

// whoWon([
//   ["O", "O", "X"],
//   ["O", "X", "X"],
//   ["X", "X", "O"],
// ]);

// whoWon([
//   ["O", "O", "X"],
//   ["X", "X", "X"],
//   ["O", "O", "O"],
// ]);

// whoWon([
//   ["O", "O", "X"],
//   ["X", "X", "O"],
//   ["O", "X", "O"],
// ]);
