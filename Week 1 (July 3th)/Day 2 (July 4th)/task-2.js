// jschallenger.com
// JS Fundamental 6 ~ 106
// JS Array       108 ~ 198
// JS Object      200 ~

// --------------------------- Javascript Fundamental ---------------------------

// // 1. Sum
// function sumFunction(a, b) {
//   return a + b;
// }

// // 2. Comparison Operators
// function compareFunction(a, b) {
//   return a === b;
// }

// // 3. Typeof Value
// function myFunction(a) {
//   return typeof a;
// }

// // 4. nth characters of string
// function myFunction(a, n) {
//   return a[n - 1];
// }

// //  5. remove first n char of string
// function myFunction(a) {
//   return a.substring(3); // bisa substring bisa slice
// }

// //  6. get last n char of string
// function myFunction(str) {
//   return str.slice(-3); // bisa substring bisa slice
// }

// // 7. get first n char of string
// function myFunction(a) {
//   return a.slice(0, 3); // bisa substring bisa slice
// }

// //  8. find the position of one string in another

// function myFunction(a) {
//   return a.indexOf("is");
// }

// // 9. extract first half of string

// function myFunction(a) {
//   return a.slice(0, a.length / 2);
// }

// // 10. remove last n char of string
// function myFunction(a) {
//   return a.slice(0, -3);
// }

// //  11. return percentage of number
// function myFunction(a, b) {
//   return (b / 100) * a;
// }

// //   12. basic javascript math operators
// function myFunction(a, b, c, d, e, f) {
//   return (((a + b - c) * d) / e) ** f;
// }

// //  13. check whether contains string and concatenate
// function myFunction(a, b) {
//   return a.indexOf(b) === -1 ? a + b : b + a;
// }

// // 14. even = true, odd = false
// function myFunction(a) {
//   return a % 2 === 0;
// }

// // 15. how many times does a character occur
// function myFunction(a, b) {
//   return b.split(a).length - 1;
// }

// // 16. check if number is a whole number
// function myFunction(a) {
//   return a - Math.floor(a) === 0;
// }

// // 17. multiplication division comparion operators
// function myFunction(a, b) {
//   return a < b ? a / b : a * b;
// }

// // 18. return number rounded to 2 decimals
// function myFunction(a) {
//   return Number(a.toFixed(2));
// }

// // 19. split the numbers to an array
// function myFunction(a) {
//   const string = a + "";
//   const strings = string.split("");
//   return strings.map((digit) => Number(digit));
// }

// ----------------------- Javascript Arrays ---------------------------

// // 1. get nth element of array
// function myFunction(a, n) {
//   return a[n - 1];
// }

// //  2. remove first n elements of an array
// function myFunction(a) {
//   return a.slice(3);
// }

// //  3. get last nth element of array
// function myFunction(a) {
//   return a.slice(-3);
// }

// //  4. get first n element of array
// function myFunction(a) {
//   return a.slice(0, 3);
// }

// //  5. return last n array elements
// function myFunction(a, n) {
//   return a.slice(-n);
// }

// //   6. remove a specific array element
// function myFunction(a, b) {
//   return a.filter((cur) => cur !== b);
// }

// // 7. counts number of elements in js array
// function myFunction(a) {
//   return a.length;
// }

// // 8. count number of negative values in array
// function myFunction(a) {
//   return a.filter((el) => el < 0).length;
// }

// // 9. Sort an array of strings alphabetically
// function myFunction(arr) {
//   return arr.sort();
// }

// // 10. sort DESCENDINGLY
// function myFunction(arr) {
//   return arr.sort((a, b) => b - a);
// }

// // 11. Calculate the sum of an array of numbers
// function myFunction(a) {
//   return a.reduce((acc, cur) => acc + cur, 0);
// }

// // 12. Return the average of an array of numbers
// function myFunction(arr) {
//   return arr.reduce((acc, cur) => acc + cur, 0) / arr.length;
// }

// // 13. return the longest string from an array of string
// function myFunction(arr) {
//   return arr.reduce((a, b) => (a.length <= b.length ? b : a));
// }

function wordLong(arr) {
  let longestWord = "";

  for (let i = 0; i < arr.length; i++) {
    if (arr[i].length > longestWord.length) {
      longestWord = arr[i];
    }
  }

  return longestWord;
}

// console.log(wordLong(["I", "need", "candy"]));

// // 14. check if all array elements are equal
// function test(arr) {
//   for (let i = 1; i < arr.length; i++) {
//     if (arr[i] !== arr[i - 1]) {
//       return false;
//     }
//   }

//   return true;
// }

let equalCheck = (arr) => {
  return [...new Set([...arr])].length === 1 ? true : false;
};

function myFunction(arr) {
  return new Set(arr).size === 1;
}

// // 15. Merge an arbitrary number of arrays
// function myFunction(...arrays) {
//   return arrays.flat();
// }

// // 16. sort array by object property (ascending for the b value)
// function myFunction(arr) {
//   return arr.sort((x, y) => x.b - y.b);
// }

// 17. merge two arrays with duplicate values
function testArrays(a, b) {
  return [...new Set([...a, ...b])].sort((x, y) => x - y);
}

// console.log(testArrays([-10, 22, 333, 42], [-11, 5, 22, 41, 42]));

// console.log(testArrays([1, 2, 3], [3, 4, 5]));

const newArr = [1, 2, 2, 2, 2, 3, 4, 5, 6, 7, 8, -22, 54, 36, 10, -29];

// console.log([...new Set(newArr)]);

// --------------------------- Javascript Object ---------------------------

// 1. accessing object properties one
function myFunction(obj) {
  return obj.country;
}

// 2. accessing obj properties two
function myFunction(obj) {
  return obj["prop-2"];
}

// 3. accessing obj properties three
function myFunction(obj, key) {
  return obj[key];
}

// 4. check if property exist in object
function myFunction(a, b) {
  return b in a;
}

// 5. check if property exist in object and is truthy
function myFunction(a, b) {
  return Boolean(a[b]);
}

// 6. creating javascript objects one
function myFunction(a) {
  return { key: a };
}

// 7. creating javascript objects two
function myFunction(a, b) {
  return { [a]: b };
}

// 8. creating javascript objects three

const entries = [
  ["a", 1],
  ["b", 2],
  ["c", 3],
];

const arr1 = ["a", "b", "c"];
const arr2 = [1, 2, 3];

// const newArr = console.log(Object.fromEntries(arr1.map((e, i) => [e, arr2[i]])));

// Jawaban dari challengernya
function myFunction(a, b) {
  return a.reduce((acc, cur, i) => ({ ...acc, [cur]: b[i] }), {});
}

// 9. extract keys from javascript object
function myFunction(a) {
  return Object.keys(a);
}

// 10. Return nested object property
function testObj(obj) {
  if (obj.a?.b !== undefined) {
    return obj.a.b;
  }

  return undefined;
}

// One Line
function myFunction(obj) {
  return obj?.a?.b;
}

// 11. Sum object values
const obj3 = {
  a: 5,
  b: 7,
};

// const valueObj3 =

// console.log(Object.values(obj3).reduce((acc, cur) => acc + cur, 0));

function myFunction(a) {
  return Object.values(a).reduce((sum, cur) => sum + cur, 0);
}

// 12. remove property from obje

const newObj = { e: 6, f: 4, b: 5, a: 3 };
const { b, ...obj12 } = newObj;

function myFunction(obj) {
  const { b, ...rest } = obj;
  return rest;
}

// 13. merge two object with matching keys
const objA = { a: 1, b: 2 };
const objB = { c: 1, b: 2, e: 3 };

let temp = objB.b;
objB.b = objA.b;

const mergedObj = { ...objA, ...objB, d: temp };

// console.log(mergedObj);

function myFunction(x, y) {
  const { b, ...rest } = y;
  return { ...x, ...rest, d: b };
}

// 14. Multiply all object values by x

// const newnewObj = console.log(Object.values(objB).map((e) => e * 3));

function test123(a, b) {
  return Object.fromEntries(Object.entries(a).map((e) => [e[0], e[1] * b]));
}

// Jawaban dr web
function myFunction(a, b) {
  return Object.entries(a).reduce((acc, [key, val]) => {
    return { ...acc, [key]: val * b };
  }, {});
}

// console.log(test123({ a: 1, b: 2, c: 3 }, 3));
// console.log(test123({ j: 9, i: 2, x: 3, z: 4 }, 10));
// console.log(test123({ w: 15, x: 22, y: 13 }, 6));

// const arrtest = [1, 2, 3, 4, 5];

// console.log(
//   arrtest.reduce((acc, cur) => {
//     console.log(`acc= ${acc}`);
//     console.log(`cur= ${cur}`);
//     return acc + cur;
//   })
// );

// --------------------------- Javascript Sets ---------------------------
// 1. check if value is present in set
function myFunction(set, val) {
  return set.has(val);
}

// 2. convert set to array
function myFunction(set) {
  return [...set];
}

// 3. get union of to sets
function myFunction(a, b) {
  const result = new Set(a);
  b.forEach((el) => result.add(el));
  return result;
}

// jawaban gua
// return new Set([...a, ...b])

// 4. creating javascript sets
function myFunction(a, b, c) {
  const set = new Set();
  set.add(a);
  set.add(b);
  set.add(c);
  return set;
}

// 5. delete element from set
function myFunction(set, val) {
  set.delete(val);
  return set;
}
