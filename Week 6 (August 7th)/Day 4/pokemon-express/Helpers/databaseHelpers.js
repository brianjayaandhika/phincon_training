import { readFileSync, writeFileSync } from "fs";

const dbPath = new URL("../Database/db.json", import.meta.url);

export const getData = () => JSON.parse(readFileSync(dbPath));
export const setData = (data) => writeFileSync(dbPath, JSON.stringify(data));

export const checkPrime = (number) => {
  if (isNaN(number) || !isFinite(number) || number % 1 || number < 2) return false;
  let m = Math.sqrt(number);
  for (let i = 2; i <= m; i++) if (number % i == 0) return false;
  return true;
};

export const fibonacci = (index) => {
  if (index === 0) return 0;
  if (index === 1) return 1;

  let prev = 0;
  let current = 1;

  for (let i = 2; i <= index; i++) {
    const next = prev + current;
    prev = current;
    current = next;
  }

  return current;
};

export const changePokemonName = (name, evolution) => {
  const splitName = name.split("-");
  const originalName = splitName[0];
  const fiboKey = fibonacci(evolution);
  return originalName + "-" + fiboKey;
};
