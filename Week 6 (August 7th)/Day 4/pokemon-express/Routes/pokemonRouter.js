import express from "express";
import pokemonController from "../Controllers/pokemonController.js";

const pokemonRouter = express.Router();

pokemonRouter.get("/", pokemonController.getAllPokemon);
pokemonRouter.get("/details/:pokemonName", pokemonController.getPokemonDetails);
pokemonRouter.post("/catch/:pokemonName", pokemonController.catchPokemon);
pokemonRouter.get("/caught", pokemonController.getCaughtPokemon);
pokemonRouter.post("/evolve", pokemonController.evolvePokemon, pokemonController.renamePokemon);

export default pokemonRouter;
