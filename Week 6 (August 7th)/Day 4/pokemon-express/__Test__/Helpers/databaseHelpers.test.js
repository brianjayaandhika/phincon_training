import { getData, setData, checkPrime, fibonacci, changePokemonName } from "../../Helpers/databaseHelpers"; // Replace with the correct path to your helper functions
import { readFileSync, writeFileSync } from "fs";

// Mock the file operations
jest.mock("fs", () => ({
  readFileSync: jest.fn(),
  writeFileSync: jest.fn(),
}));

// Mock the existing data in the "db.json" file
const mockDbData = {
  myPokemon: [
    { id: "5744de74-8654-45b9-8cb1-6194713b222e", name: "squirtle-55", evolution: 11 },
    { id: "cc333ee9-cbcc-402b-a0c4-8e8ac6552575", name: "bulbasaur", evolution: 0 },
    { id: "edb12a31-e97a-4ea3-9d46-dc8b893f6a79", name: "charmander", evolution: 0 },
    { id: "f82f3b1c-6076-4011-9c29-f4107c5f86b3", name: "charizard", evolution: 0 },
    { id: "b6341278-be82-4817-9dda-8836886a3789", name: "dog", evolution: 0 },
    { id: "6089941c-cd49-4d39-9ee2-773194da67a4", name: "rayquaza", evolution: 0 },
    { id: "813eec5f-83ce-44a0-8a5e-75350d852a3d", name: "rayquaza", evolution: 0 },
  ],
};

// Mock the initial state of the file
jest.spyOn(JSON, "parse").mockReturnValue(mockDbData);

describe("getData", () => {
  it("should read data from the file", () => {
    const data = getData();
    expect(data).toEqual(mockDbData);
  });
});

describe("setData", () => {
  it("should write data to the file", () => {
    const newData = { myPokemon: [] };
    setData(newData);
    expect(writeFileSync).toHaveBeenCalledWith(expect.anything(), JSON.stringify(newData));
  });
});

describe("checkPrime", () => {
  it("should return true for prime numbers", () => {
    expect(checkPrime(2)).toBe(true);
    expect(checkPrime(7)).toBe(true);
    // Add more test cases
  });

  it("should return false for non-prime numbers", () => {
    expect(checkPrime(1)).toBe(false);
    expect(checkPrime(4)).toBe(false);
    // Add more test cases
  });
});

describe("fibonacci", () => {
  it("should return the correct Fibonacci number", () => {
    expect(fibonacci(0)).toBe(0);
    expect(fibonacci(1)).toBe(1);
    expect(fibonacci(5)).toBe(5);
    // Add more test cases
  });
});

describe("changePokemonName", () => {
  it("should rename a Pokemon", () => {
    expect(changePokemonName("squirtle", 5)).toBe("squirtle-5");
    // Add more test cases
  });
});
