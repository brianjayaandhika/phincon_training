import axios from "axios";
import pokemonController from "../../Controllers/pokemonController.js";
import responseHelpers from "../../Helpers/responseHelpers.js";
import { getData, setData, checkPrime, changePokemonName } from "../../Helpers/databaseHelpers.js";

// Mock the axios library
jest.mock("axios");
jest.mock("../../Helpers/responseHelpers");
jest.mock("../../Helpers/databaseHelpers");
jest.mock("uuid", () => ({
  v4: jest.fn(() => "mocked_uuid"),
}));

describe("pokemonController", () => {
  let mockReq;
  let mockRes;
  let mockNext;

  beforeEach(() => {
    mockReq = { params: {}, body: { id: "5744de74-8654-45b9-8cb1-6194713b222e" }, dataFromEvolvePokemon: { data: { myPokemon: [] } } };
    mockRes = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
      locals: {},
    };
    mockNext = jest.fn();
    jest.clearAllMocks();
  });

  describe("getAllPokemon", () => {
    it("should return a response with status code, message, and data from the PokeAPI", async () => {
      // Mock the axios.get function to return a specific response
      const mockResponse = {
        data: {
          results: [{ name: "Pikachu" }, { name: "Charmander" }],
        },
      };
      axios.get.mockResolvedValue(mockResponse);

      // Call the getAllPokemon function
      await pokemonController.getAllPokemon(mockReq, mockRes);

      // Verify that axios.get is called with the correct URL
      expect(axios.get).toHaveBeenCalledWith("https://pokeapi.co/api/v2/pokemon/?limit=100");

      // Verify that responseHelper is called with the expected arguments
      expect(responseHelpers).toHaveBeenCalledWith(mockRes, 200, ["Pikachu", "Charmander"], "Successfully get all pokemon names");
    });

    it("should handle API error and return a 500 response", async () => {
      axios.get.mockRejectedValue(new Error("Some error"));

      await pokemonController.getAllPokemon(mockReq, mockRes);

      expect(axios.get).toHaveBeenCalledWith("https://pokeapi.co/api/v2/pokemon/?limit=100");
      expect(responseHelpers).toHaveBeenCalledWith(mockRes, 500);
    });
  });

  describe("getPokemonDetails", () => {
    it("should return Pokemon details with status code 200", async () => {
      const pokemonName = "pikachu";
      const mockResponse = {
        data: {
          name: "Pikachu",
          weight: 60,
          height: 40,
          location_area_encounters: "some_location",
          types: [{ type: { name: "electric" } }],
        },
      };

      axios.get.mockResolvedValue(mockResponse);
      mockReq.params.pokemonName = pokemonName;

      await pokemonController.getPokemonDetails(mockReq, mockRes);

      expect(axios.get).toHaveBeenCalledWith(`https://pokeapi.co/api/v2/pokemon/${pokemonName}`);
      expect(responseHelpers).toHaveBeenCalledWith(mockRes, 200, {
        name: "Pikachu",
        weight: 60,
        height: 40,
        location: "some_location",
        type: "electric",
      });
    });

    it("should handle API error (404) and return a 404 response", async () => {
      const pokemonName = "non_existent_pokemon";
      const mockErrorResponse = {
        response: {
          status: 404,
        },
      };

      axios.get.mockRejectedValue(mockErrorResponse);
      mockReq.params.pokemonName = pokemonName;

      await pokemonController.getPokemonDetails(mockReq, mockRes);

      expect(axios.get).toHaveBeenCalledWith(`https://pokeapi.co/api/v2/pokemon/${pokemonName}`);
      expect(responseHelpers).toHaveBeenCalledWith(mockRes, 404, null, "API not found");
    });

    it("should handle other API errors and return a 500 response", async () => {
      const pokemonName = "pikachu";
      axios.get.mockRejectedValue(new Error("Some error"));
      mockReq.params.pokemonName = pokemonName;

      await pokemonController.getPokemonDetails(mockReq, mockRes);

      expect(axios.get).toHaveBeenCalledWith(`https://pokeapi.co/api/v2/pokemon/${pokemonName}`);
      expect(responseHelpers).toHaveBeenCalledWith(mockRes, 500);
    });
  });

  describe("catchPokemon", () => {
    it("should successfully catch a Pokemon", async () => {
      const pokemonName = "articuno";
      const mockData = { myPokemon: [] };
      getData.mockReturnValue(mockData);
      axios.get.mockResolvedValue({});

      // Use a fixed UUID for consistent testing

      const expectedNewPokemon = {
        id: "mocked_uuid",
        name: pokemonName,
        evolution: 0,
      };

      Math.random = jest.fn().mockReturnValue(0.7); // Mock a high random value for successful catch

      mockReq.params.pokemonName = pokemonName; // Set the pokemonName in mockReq

      await pokemonController.catchPokemon(mockReq, mockRes);

      expect(getData).toHaveBeenCalled();
      expect(setData).toHaveBeenCalledWith({ myPokemon: [expectedNewPokemon] });
      expect(responseHelpers).toHaveBeenCalledWith(mockRes, 200, expectedNewPokemon, `Nice! ${pokemonName} has been caught!`);
    });

    it("should not catch a Pokemon", async () => {
      const pokemonName = "articuno";
      const mockData = { myPokemon: [] };
      getData.mockReturnValue(mockData);
      axios.get.mockResolvedValue({});
      const expectedNewPokemon = {
        id: "mocked_uuid",
        name: pokemonName,
        evolution: 0,
      };

      Math.random = jest.fn().mockReturnValue(0.3); // Mock a low random value for unsuccessful catch
      mockReq.params.pokemonName = pokemonName;

      await pokemonController.catchPokemon(mockReq, mockRes);

      expect(getData).toHaveBeenCalled();
      expect(setData).not.toHaveBeenCalled(); // No setData because the Pokemon was not caught
      expect(responseHelpers).toHaveBeenCalledWith(mockRes, 201, expectedNewPokemon, `Unlucky! ${pokemonName} has not been caught!`);
    });

    it("should handle an error and return a 500 response", async () => {
      getData.mockImplementation(() => {
        throw new Error("Some error");
      });

      await pokemonController.catchPokemon(mockReq, mockRes);

      expect(getData).toHaveBeenCalled();
      expect(responseHelpers).toHaveBeenCalledWith(mockRes, 500);
    });
  });

  describe("getCaughtPokemon", () => {
    it("should successfully get caught Pokemon and show responseclear", async () => {
      const mockData = {
        myPokemon: [
          { id: "5744de74-8654-45b9-8cb1-6194713b222e", name: "squirtle-89", evolution: 12 },
          { id: "cc333ee9-cbcc-402b-a0c4-8e8ac6552575", name: "bulbasaur", evolution: 0 },
          // ... other mock data
        ],
      };
      getData.mockReturnValue(mockData);

      await pokemonController.getCaughtPokemon(mockReq, mockRes);

      expect(getData).toHaveBeenCalled();
      expect(responseHelpers).toHaveBeenCalledWith(mockRes, 200, mockData.myPokemon);
    });

    it("should handle error and return a 500 response", async () => {
      getData.mockImplementation(() => {
        throw new Error("Some error");
      });

      await pokemonController.getCaughtPokemon(mockReq, mockRes);

      expect(getData).toHaveBeenCalled();
      expect(responseHelpers).toHaveBeenCalledWith(mockRes, 500);
    });
  });

  describe("evolvePokemon", () => {
    it("should successfully evolve a Pokemon", async () => {
      const mockData = {
        myPokemon: [
          { id: "5744de74-8654-45b9-8cb1-6194713b222e", name: "squirtle-89", evolution: 12 },
          // ... other mock data
        ],
      };
      const mockSelectedId = "5744de74-8654-45b9-8cb1-6194713b222e";
      getData.mockReturnValue(mockData);
      checkPrime.mockReturnValue(true);

      await pokemonController.evolvePokemon(mockReq, mockRes, mockNext);

      expect(getData).toHaveBeenCalled();
      expect(checkPrime).toHaveBeenCalled();
      expect(mockReq.dataFromEvolvePokemon).toEqual({ data: mockData, selectedId: mockSelectedId });
      expect(mockNext).toHaveBeenCalled();
    });

    it("should handle failed evolution and return a 201 response", async () => {
      const mockData = {
        myPokemon: [
          { id: "5744de74-8654-45b9-8cb1-6194713b222e", name: "squirtle-89", evolution: 12 },
          // ... other mock data
        ],
      };
      const mockSelectedId = "5744de74-8654-45b9-8cb1-6194713b222e";
      getData.mockReturnValue(mockData);
      checkPrime.mockReturnValue(false);

      await pokemonController.evolvePokemon(mockReq, mockRes, mockNext);

      expect(getData).toHaveBeenCalled();
      expect(checkPrime).toHaveBeenCalled();
      expect(mockReq.dataFromEvolvePokemon).toEqual({ data: mockData, selectedId: mockSelectedId });
      expect(responseHelpers).toHaveBeenCalledWith(mockRes, 201, null, "Evolution Failed!");
    });

    it("should handle a case where the Pokemon with the specified ID is not found", async () => {
      const mockData = {
        myPokemon: [
          { id: "5744de74-8654-45b9-8cb1-6194713b222e", name: "squirtle-89", evolution: 12 },
          // ... other mock data
        ],
      };
      const mockSelectedId = "invalid-id";
      getData.mockReturnValue(mockData);
      checkPrime.mockReturnValue(false);
      mockReq.body.id = mockSelectedId;

      await pokemonController.evolvePokemon(mockReq, mockRes, mockNext);

      expect(getData).toHaveBeenCalled();
      expect(checkPrime).toHaveBeenCalled();
      expect(mockReq.dataFromEvolvePokemon).toEqual({ data: mockData, selectedId: mockSelectedId });
      expect(responseHelpers).toHaveBeenCalledWith(mockRes, 400, null, `cant find pokemon with id: ${mockSelectedId}`);
    });

    it("should handle an error and return a 500 response", async () => {
      getData.mockImplementation(() => {
        throw new Error("Some error");
      });

      await pokemonController.evolvePokemon(mockReq, mockRes, mockNext);

      expect(getData).toHaveBeenCalled();
      expect(responseHelpers).toHaveBeenCalledWith(mockRes, 500);
    });
  });

  //   STILL NOT WORKING!!!
  describe("renamePokemon", () => {
    it("should rename a Pokemon successfully", () => {
      const mockMyPokemon = [{ id: "mocked_uuid", name: "Pikachu", evolution: 0 }];
      getData.mockReturnValue(mockMyPokemon);

      changePokemonName.mockReturnValue("Pikachu-0");

      pokemonController.renamePokemon(mockReq, mockRes);

      // Verify that setData is called with the expected argument
      expect(setData).toHaveBeenCalledWith({ myPokemon: [{ id: "mocked_uuid", name: "Pikachu-1", evolution: 1 }] });
      expect(responseHelper).toHaveBeenCalledWith(mockRes, 200, { id: "mocked_uuid", name: "Pikachu-1", evolution: 1 }, "Success rename pokemon");
    });
  });
});
