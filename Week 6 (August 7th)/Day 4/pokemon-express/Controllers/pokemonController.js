import axios from "axios";
import responseHelper from "../Helpers/responseHelpers.js";
import { v4 as uuidv4 } from "uuid";
import { getData, setData, checkPrime, changePokemonName } from "../Helpers/databaseHelpers.js";

const pokemonController = {
  // Nomor 1
  getAllPokemon: async (req, res) => {
    try {
      const response = await axios.get("https://pokeapi.co/api/v2/pokemon/?limit=100");

      const pokemonName = response?.data?.results?.map((e) => e.name);
      responseHelper(res, 200, pokemonName, "Successfully get all pokemon names");
    } catch (error) {
      responseHelper(res, 500);
    }
  },

  // Nomor 2
  getPokemonDetails: async (req, res) => {
    try {
      const pokemonName = req.params.pokemonName;
      const response = await axios.get(`https://pokeapi.co/api/v2/pokemon/${pokemonName}`);

      const specificData = {
        name: response?.data?.name,
        weight: response?.data?.weight,
        height: response?.data?.height,
        location: response?.data?.location_area_encounters,
        type: response?.data?.types.map((type) => type?.type?.name).join(" - "),
      };

      responseHelper(res, 200, specificData);
    } catch (error) {
      const errStatus = error?.response?.status;

      if (errStatus === 404) {
        responseHelper(res, 404, null, `API not found`);
      }

      responseHelper(res, 500);
    }
  },

  // Nomor 3
  catchPokemon: (req, res) => {
    try {
      const pokemonName = req.params.pokemonName;
      const data = getData();
      const newPokemon = {
        id: uuidv4(),
        name: pokemonName,
        evolution: 0,
      };

      if (Math.floor(Math.random() * 10) >= 5) {
        setData({ myPokemon: [...data.myPokemon, newPokemon] });
        responseHelper(res, 200, newPokemon, `Nice! ${pokemonName} has been caught!`);
      } else {
        responseHelper(res, 201, newPokemon, `Unlucky! ${pokemonName} has not been caught!`);
      }
    } catch (error) {
      responseHelper(res, 500);
    }
  },

  // Nomor 4
  getCaughtPokemon: (req, res) => {
    try {
      const data = getData();
      responseHelper(res, 200, data.myPokemon);
    } catch (error) {
      responseHelper(res, 500);
    }
  },

  // Nomor 5
  evolvePokemon: (req, res, next) => {
    try {
      const data = getData();
      const selectedId = req.body.id;
      const passedData = { data, selectedId };
      req.dataFromEvolvePokemon = passedData;

      const myCaughtPokemon = data.myPokemon.find((e) => e.id == selectedId);

      const isPrime = checkPrime(Math.floor(Math.random() * 10 + 1));

      if (isPrime) {
        next();
      } else {
        if (myCaughtPokemon) {
          responseHelper(res, 201, null, `Evolution Failed!`);
        }
        responseHelper(res, 400, null, `cant find pokemon with id: ${selectedId}`);
      }
    } catch (error) {
      responseHelper(res, 500);
    }
  },

  // Nomor 6
  renamePokemon: (req, res) => {
    try {
      const data = req.dataFromEvolvePokemon.data.myPokemon;
      const id = req.dataFromEvolvePokemon.selectedId;
      const selectedPokemon = data.find((e) => e.id == id);

      if (selectedPokemon) {
        const newData = data.map((item) => {
          if (item.id == id) {
            item.name = changePokemonName(item.name, item.evolution);
            item.evolution += 1;
          }
          return item;
        });
        setData({ myPokemon: [...newData] });
        responseHelper(res, 200, null, `Pokemon has been renamed to ${selectedPokemon.name}`);
      }
    } catch (error) {
      responseHelper(res, 500);
    }
  },
};

export default pokemonController;
