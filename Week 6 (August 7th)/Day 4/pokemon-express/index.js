import express from "express";
import pokemonRouter from "./Routes/pokemonRouter.js";

const app = express();
const port = process.env.PORT || 3000;
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use("/api/pokemon", pokemonRouter);

app.all("*", (req, res) => {
  res.status(404).json({
    message: "API not found",
  });
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
