import express from "express";
import fs, { writeFileSync } from "fs";
import bodyParser from "body-parser";

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false })); // belom bisa pake

const port = 3000;
const database = "./Database/db.json";
const data = JSON.parse(fs.readFileSync(database));
const jsonParser = bodyParser.json();

const STATUS_SUCCESS = 200;
const STATUS_NOT_FOUND = 404;
const STATUS_INTERNAL_ERROR = 500;
const STATUS_BAD_REQUEST = 400;

// Utility function for handling errors
function handleServerError(res, message) {
  res.status(STATUS_INTERNAL_ERROR).json({ message: message || "Internal Server Error" });
}

function handleClientError(res, status, message) {
  res.status(status).json({ message });
}

// _______________________________________________________________

// All endpoints

// GET All Data - get("/all")
// GET All Animal Data - get("/all/animal")
// GET All Mammal data - get("/all/animal/mammal")
// GET Specific data of Animal - get("/all/animal/mammal/cow")

// POST Create Data - get("/all/animal/mammal") - body = name and description

// PUT Edit Data - get("/all/animal/mammal/cow") - body = description

// DELETE Delete Data - get("/all/animal/fish") - body = name

// _______________________________________________________________
// All functions

// GET - All data
app.get("/all", (req, res) => {
  try {
    res.status(STATUS_SUCCESS).json({ data, message: "success" });
  } catch (error) {
    handleServerError(res);
  }
});

// GET - Data of Animal / Plant
app.get("/all/:thing", (req, res) => {
  try {
    const selectedParam = req.params.thing;

    if (selectedParam !== "animal" && selectedParam !== "plant") {
      return handleClientError(res, STATUS_NOT_FOUND, "URL not found");
    }

    res.status(STATUS_SUCCESS).json({ data: data[selectedParam], message: "success" });
  } catch (error) {
    handleServerError(res);
  }
});

// GET - Data of animal / plant with that species
app.get("/all/:thing/:species", (req, res) => {
  try {
    const species = req.params.species;
    const selectedParam = req.params.thing;

    if (selectedParam !== "animal" && selectedParam !== "plant") {
      return handleClientError(res, STATUS_NOT_FOUND, "URL not found");
    }

    if (!data[selectedParam][species]) {
      return handleClientError(res, STATUS_NOT_FOUND, "URL not found");
    }

    res.status(STATUS_SUCCESS).json({ [species]: data[selectedParam][species], message: "success" });
  } catch (error) {
    handleServerError(res);
  }
});

// GET - Data of Specific Animal / Plant
app.get("/all/:thing/:species/:specific", (req, res) => {
  try {
    const species = req.params.species;
    const selectedParam = req.params.thing;
    const specificThing = req.params.specific;

    if (selectedParam !== "animal" && selectedParam !== "plant") {
      return handleClientError(res, STATUS_NOT_FOUND, "URL not found");
    }

    if (!data[selectedParam][species] === undefined) {
      return handleClientError(res, STATUS_NOT_FOUND, "URL not found");
    }

    if (!data[selectedParam][species].find((e) => e.name == specificThing)) {
      handleClientError(res, STATUS_NOT_FOUND, "URL not found");
    }

    const thingDetail = data[selectedParam][species]
      .filter((item) => item.name === specificThing)
      .map((item) => item.description)
      .join("");

    res.status(STATUS_SUCCESS).json({
      description: thingDetail,
      message: "success",
    });
  } catch (error) {
    handleServerError(res);
  }
});

// POST - Create new animal / plant
app.post("/all/:thing/:species", jsonParser, (req, res) => {
  try {
    const species = req.params.species;
    const selectedParam = req.params.thing;
    const newThing = req.body;

    if (Object.keys(newThing).length < 2) {
      res.status(STATUS_BAD_REQUEST).json({ message: "Body error" });
    }

    if (newThing.description === "") {
      newThing.description = "An animal that the admin forgot to give description to.";
    }

    if (!data[selectedParam][species].find((e) => e.name === newThing.name)) {
      data[selectedParam][species].push(newThing);
      fs.writeFileSync(database, JSON.stringify(data));
    } else {
      res.status(STATUS_BAD_REQUEST).json({ message: "Data already existed" });
    }

    res.status(STATUS_SUCCESS).json({ [species]: data[selectedParam][species], message: "success" });
  } catch (error) {
    handleServerError(res);
  }
});

// PUT - Edit animal / plant
app.put("/all/:thing/:species/:specific", jsonParser, (req, res) => {
  try {
    const species = req.params.species;
    const selectedParam = req.params.thing;
    const specificThing = req.params.specific;
    const newThing = req.body;

    if (!data[selectedParam][species].find((e) => e.name === specificThing)) {
      res.status(STATUS_BAD_REQUEST).json({ message: "Data not find" });
    }

    const newArray = data[selectedParam][species].filter((e) => e.name === specificThing)[0];

    newArray.description = newThing.description;

    fs.writeFileSync(database, JSON.stringify(data));

    res.status(STATUS_SUCCESS).json({ [species]: data[selectedParam][species], message: "success" });
  } catch (error) {
    handleServerError(res);
  }
});

// DELETE - Delete animal / plant
app.delete("/all/:thing/:species", jsonParser, (req, res) => {
  try {
    const species = req.params.species;
    const selectedParam = req.params.thing;
    const selectedThing = req.body;

    if (!data[selectedParam][species].find((e) => e.name == selectedThing.name)) {
      handleClientError(res, STATUS_NOT_FOUND, "Data not found");
    }

    const updatedArray = data[selectedParam][species].filter((item) => item.name !== selectedThing.name);

    data[selectedParam][species] = updatedArray;

    fs.writeFileSync(database, JSON.stringify(data));

    res.status(STATUS_SUCCESS).json({ [species]: data[selectedParam][species], message: `${selectedThing.name} has been deleted` });
  } catch (error) {
    handleServerError(res);
  }
});

app.all("*", (req, res) => {
  res.status(404).json({
    message: "API not found",
  });
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
