const numbBtn = document.querySelectorAll(".btn-numb");
const display = document.querySelector(".display");
const prevNumb = document.querySelector(".prev-numb");
const operBtn = document.querySelectorAll(".btn-operator");
const delBtn = document.querySelector(".btn-del");
const resetBtn = document.querySelector(".btn-reset");
const equalBtn = document.querySelector(".btn-equal");

const otherBtn = ["RESET", "DEL", "="];
const operationBtn = ["+", "-", "*", "/", "="];
let operation = "";
let prev = 0;
let result = 0;

display.innerHTML = "";

numbBtn.forEach((e) => {
  e.addEventListener("click", (e) => {
    if (display.innerHTML.length === 0) {
      if (e.target.innerHTML === ".") {
        display.innerHTML = 0;
      }
    }

    if (display.innerHTML === "0") {
      if (e.target.innerHTML !== ".") {
        return -1;
      }
    }

    if (display.innerHTML.length > 16) {
      alert("Too much character!");
      return -1;
    }

    if (otherBtn.includes(e.target.innerHTML)) {
      return -1;
    } else if (display.innerHTML.includes(".")) {
      if (e.target.innerHTML === ".") {
        null;
      } else {
        display.innerHTML += e.target.innerHTML;
      }
    } else {
      display.innerHTML += e.target.innerHTML;
    }
  });
});

// Clear function
resetBtn.addEventListener("click", (e) => {
  display.innerHTML = "";
  prev = "";
  prevNumb.innerHTML = prev;
  result = 0;
  operation = "";
});

// Delete function
delBtn.addEventListener("click", (e) => {
  display.innerHTML = display.innerHTML.slice(0, display.innerHTML.length - 1);
});

// Operation function
operBtn.forEach((e) => {
  e.addEventListener("click", (e) => {
    if (display.innerHTML.length === 0) {
      prev = "";
    } else {
      prev = parseFloat(display.innerHTML);
    }
    display.innerHTML = "";
    prevNumb.innerHTML = prev;

    operation = e.target.innerHTML;
  });
});

// Equal function
equalBtn.addEventListener("click", (e) => {
  let curr = parseFloat(display.innerHTML);

  if (result !== 0) {
    prev = result;
  }

  if (display.innerHTML === "") {
    display.innerHTML === prev;
  } else {
    switch (operation) {
      case "+":
        result = prev + curr;
        break;
      case "-":
        result = prev - curr;
        break;
      case "x":
        result = prev * curr;
        break;
      case "/":
        result = prev / curr;
        break;
      default:
        result = prev;
    }

    display.innerHTML = result;
    prev = "";
    if (prevNumb.innerHTML === NaN) {
      prevNumb.innerHTML = "";
    } else {
      prevNumb.innerHTML = prev;
    }
  }
});

// Randomize Background Button

const randomBtn = document.querySelector(".top-layout-btn");
randomBtn.addEventListener("click", () => {
  let random1 = `rgba(${Math.random() * 254 + 1}, ${Math.random() * 254 + 1}, ${Math.random() * 254 + 1}, 0.6)`;
  let random2 = `rgba(${Math.random() * 254 + 1}, ${Math.random() * 254 + 1}, ${Math.random() * 254 + 1}, 0.6)`;
  let random3 = `rgba(${Math.random() * 254 + 1}, ${Math.random() * 254 + 1}, ${Math.random() * 254 + 1}, 0.6)`;

  let red = Math.random() * 254 + 1;
  let blue = Math.random() * 254 + 1;
  let green = Math.random() * 254 + 1;

  document.getElementById("outerContainer").style.backgroundColor = `rgb(${red}, ${green}, ${blue})`;
  bg.style.backgroundColor = `rgb(${red}, ${green}, ${blue})`;
  bg.style.color = `rgb(${red - 100}, ${green - 100}, ${blue - 100})`;

  document.getElementById("buttonContainer").style.backgroundColor = "rgba(0,0,0,0.6)";
  document.getElementById("legendTextContainer").style.color = "#ffffff";
  document.querySelector(".top-layout-text").style.color = "#ffffff";
  document.querySelector(".redButton").style.backgroundColor = random3;
  document.querySelector(".prev-numb").style.color = "#ffffff";
  document.querySelector(".theme-text").style.color = "#ffffff";
  document.querySelector(".top-layout-btn").style.color = "#ffffff";

  display.style.backgroundColor = `rgba(0,0,0, 0.6)`;
  display.style.color = "#ffffff";

  calcBg.style.backgroundColor = "`rgba(0,0,0, 0.6)`";
  numbBtn.forEach((e) => {
    e.style.backgroundColor = `rgba(${red}, ${green}, ${blue}, 0.7)`;
    e.style.color = "white";
  });
  operBtn.forEach((e) => {
    e.style.backgroundColor = random3;
    e.style.color = "white";
  });
  [delBtn, resetBtn].forEach((e) => {
    e.style.backgroundColor = random1;
    e.style.color = "#ffffff";
  });
  equalBtn.style.backgroundColor = random2;
  equalBtn.style.color = "#ffffff";
});

// Theme Switch
const bg = document.querySelector(".app-container");
const calcBg = document.querySelector(".buttons");

let toggleSwitch = document.getElementsByClassName("redButton")[0];

// background, display, buttons (bg + font), del & reset, equal, calc layout, warna tulisan

function go_to_1() {
  toggleSwitch.classList.add("horizTranslate1");
  toggleSwitch.classList.remove("horizTranslate2");
  toggleSwitch.classList.remove("horizTranslate3");

  //   Style
  document.getElementById("outerContainer").style.backgroundColor = "rgba(59, 70, 100, 1)";
  document.getElementById("buttonContainer").style.backgroundColor = "rgba(0,0,0,0.6)";
  document.getElementById("legendTextContainer").style.color = "#ffffff";
  document.querySelector(".redButton").style.backgroundColor = "#cc5200";

  document.querySelector(".top-layout-text").style.color = "#ffffff";
  document.querySelector(".prev-numb").style.color = "#ffffff";
  document.querySelector(".theme-text").style.color = "#ffffff";
  document.querySelector(".top-layout-btn").style.color = "#ffffff";

  bg.style.backgroundColor = "rgba(59, 70, 100, 1)";
  bg.style.color = "rgba(234, 227, 219, 1)";
  display.style.backgroundColor = "rgba(24, 31, 50, 1)";
  display.style.color = "#ffffff";

  calcBg.style.backgroundColor = "rgba(37, 45, 68, 1)";
  numbBtn.forEach((e) => {
    e.style.backgroundColor = "rgba(234, 227, 219, 1)";
    e.style.color = "rgba(72, 72, 88, 1)";
  });
  [delBtn, resetBtn].forEach((e) => {
    e.style.backgroundColor = "rgba(100, 114, 153, 1)";
    e.style.color = "rgba(234, 227, 219, 1)";
  });
  equalBtn.style.backgroundColor = "rgba(209, 63, 50, 1)";
  equalBtn.style.color = "rgba(234, 227, 219, 1)";
}

function go_to_2() {
  toggleSwitch.classList.add("horizTranslate2");
  toggleSwitch.classList.remove("horizTranslate3");
  toggleSwitch.classList.remove("horizTranslate1");

  document.getElementById("outerContainer").style.backgroundColor = "#e6e6e6";
  document.getElementById("buttonContainer").style.backgroundColor = "#d3cdcd";
  document.getElementById("legendTextContainer").style.color = "#3a3934";
  document.querySelector(".redButton").style.backgroundColor = "#cc5200";
  document.querySelector(".theme-text").style.color = "#3a3934";
  document.querySelector(".top-layout-btn").style.color = "#3a3934";

  bg.style.backgroundColor = "#e6e6e6";
  document.querySelector(".app-container").style.color = "#3a3934";
  document.querySelector(".top-layout-text").style.color = "#3a3934";
  document.querySelector(".prev-numb").style.color = "#3a3934";
  display.style.backgroundColor = "#eeeeee";
  display.style.color = "#3b3a31";
  calcBg.style.backgroundColor = "#d3cdcd";
  numbBtn.forEach((e) => {
    e.style.backgroundColor = "#e5e4e0";
    e.style.color = "#3b3a31";
  });
  [delBtn, resetBtn].forEach((e) => {
    e.style.backgroundColor = "#388187";
    e.style.color = "#daf4f8";
  });
  equalBtn.style.backgroundColor = "#c85401";
  equalBtn.style.color = "#daf4f8";
}

function go_to_3() {
  toggleSwitch.classList.add("horizTranslate3");
  toggleSwitch.classList.remove("horizTranslate2");
  toggleSwitch.classList.remove("horizTranslate1");
  document.getElementById("outerContainer").style.backgroundColor = "#17062a";
  document.getElementById("buttonContainer").style.backgroundColor = "#1e0836";
  document.getElementById("legendTextContainer").style.color = "#f5e84d";
  document.querySelector(".prev-numb").style.color = "#f5e84d";
  document.querySelector(".top-layout-btn").style.color = "#f5e84d";

  document.querySelector(".redButton").style.backgroundColor = "#08dacf";
  document.querySelector(".theme-text").style.color = "#f5e84d";

  bg.style.backgroundColor = "#17062a";
  document.querySelector(".app-container").style.color = "# f5e84d";
  document.querySelector(".top-layout-text").style.color = "#f5e84d";
  display.style.backgroundColor = "#1e0836";
  display.style.color = "#f5e84d";
  calcBg.style.backgroundColor = "#1e0836";
  numbBtn.forEach((e) => {
    e.style.backgroundColor = "#331b4d";
    e.style.color = "#f5e84d    ";
  });
  [delBtn, resetBtn].forEach((e) => {
    e.style.backgroundColor = "#580777";
    e.style.color = "#ffe3ff";
  });
  equalBtn.style.backgroundColor = "#00decf";
  equalBtn.style.color = "#004743";
}
