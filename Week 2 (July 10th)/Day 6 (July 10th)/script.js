const unreadyFeature = document.querySelectorAll(".not-ready");

unreadyFeature.forEach((e) => {
  e.addEventListener("click", function () {
    alert("Feature not ready!");
  });
});
