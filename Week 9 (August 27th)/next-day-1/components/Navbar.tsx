"use client";

import Link from "next/link";
import Image from "next/image";
import cartIconBlack from "../public/icon-cart-black.svg";
import trashIcon from "../public/icon-delete.svg";
import profileImage from "../public/image-avatar.png";
import burgerMenu from "../public/icon-menu.svg";
import closeIcon from "../public/icon-close.svg";

import { useAppSelector, useAppDispatch } from "../redux/hooks";
import { deleteProduct } from "@/redux/features/productSlices";
import { useState } from "react";
import { Product } from "@/redux/features/productSlices";

const Navbar = () => {
  const [openBubble, setOpenBubble] = useState<boolean>(false);
  const [openMenu, setOpenMenu] = useState<boolean>(false);

  // Hooks
  const dispatch = useAppDispatch();
  const selectedProduct = useAppSelector((state) => state?.product?.value);

  const navbarItems = ["Collections", "Men", "Women", "About", "Contact"].map((item) => (
    <li key={item}>
      <Link className="opacity-75 ease-in-out hover:opacity-100 hover:border-b-4 border-orange-400 pb-9" href={`/${item.toLowerCase()}`}>
        {item}
      </Link>
    </li>
  ));

  // handle open bubble
  const handleOpenBubble = () => {
    setOpenBubble(!openBubble);
  };

  // handle open Menu
  const handleOpenMenu = () => {
    setOpenMenu(!openMenu);
  };

  // handle delete product
  const handleDeleteProduct = (id: string): void => {
    dispatch(deleteProduct(id));
  };

  return (
    <nav className="py-7 flex items-center justify-between px-2 lg:px-0 ">
      {/* Mobile */}
      <div className="flex gap-6 items-center">
        <button className="md:hidden flex items-center mt-1" onClick={() => handleOpenMenu()}>
          <Image src={burgerMenu} alt="menu" width={18} />
        </button>
        <Link href="/" className="text-3xl md:ms-0 font-bold no-underline text-black-opacity-75 tracking-tight">
          sneakers
        </Link>
      </div>
      {/* Desktop */}
      <ul className="text-black-opacity-75 items-center gap-8 xl:-ms-72 lg:-ms-68 hidden md:flex">{navbarItems}</ul>
      <div className="flex items-center gap-8">
        <button className="hover:opacity-80" onClick={() => handleOpenBubble()}>
          <Image src={cartIconBlack} alt="cart icon" className="w-4 h-4 text-red-400" />
        </button>
        <button>
          <Image src={profileImage} alt="cart icon" className="w-10 h-10 text-red-400 hover:border-2 border-orange-400 rounded-full" />
        </button>
      </div>
      {/* Speech bubble for cart on click */}
      {openBubble && (
        <div className="absolute top-28 md:top-20 md:right-24 right-4 z-50 md:w-64 w-11/12 rounded-lg shadow-xl bg-white ">
          <div className="flex justify-between items-center h-8 w-fit p-4 py-6">
            <h1 className="text-black font-bold">Cart</h1>
          </div>
          <hr />
          <div className={`flex flex-col items-center h-fit gap-3 px-3 py-6 `}>
            {selectedProduct.length > 0 ? (
              selectedProduct.map((product: Product) => (
                <div key={product.id} className="flex items-center w-full justify-between">
                  <Image src={product.image} alt="thumbnail" width={42} height={42} className="rounded-md" />
                  <div className="flex flex-col">
                    <p className="text-black text-opacity-60 text-sm">{product.name}</p>
                    <div className="flex gap-1">
                      <p className="text-black text-opacity-60 text-sm">${product.singlePrice}.00</p>
                      <span className="text-black text-opacity-60 text-sm">x</span>
                      <p className="text-black text-opacity-60 text-sm">{product.amount}</p>
                      <p className="text-black text-sm font-bold">${product.singlePrice * product.amount}.00</p>
                    </div>
                  </div>
                  <button onClick={(): void => handleDeleteProduct(product.id)}>
                    <Image src={trashIcon} alt={"delete button"} />
                  </button>
                </div>
              ))
            ) : (
              <p className="text-center text-black">Your cart is empty</p>
            )}
            <button className={`w-full text-white bg-orange-500 py-3 rounded-lg ${selectedProduct.length > 0 ? "" : "hidden"}`}>Checkout</button>
          </div>
        </div>
      )}
      {/* MENU FOR MOBILE */}
      {openMenu && (
        <div className="fixed top-0 left-0 w-full z-50 h-screen bg-black-opacity-75">
          <div className="bg-white w-full absolute top-0 left-0 z-50 h-full p-4 ">
            <button className="flex items-center pt-4" onClick={() => handleOpenMenu()}>
              <Image src={closeIcon} alt="close" width={24} />
            </button>
            <ul className="text-black-opacity-75 items-center flex flex-col gap-8 mt-12 text-xl font-bold">{navbarItems}</ul>
          </div>
        </div>
      )}
    </nav>
  );
};

export default Navbar;
