"use client";

import { createSlice, PayloadAction, current } from "@reduxjs/toolkit";
import { StaticImageData } from "next/image";

export interface Product {
  id: string;
  name: string;
  singlePrice: number;
  amount: number;
  image: StaticImageData;
}

export interface ProductState {
  value: Product[];
}

const initialState: ProductState = { value: [] };

export const productSlice = createSlice({
  name: "product",
  initialState,
  reducers: {
    addProduct: (state: any, action: PayloadAction<ProductState>) => {
      const currentState = current(state);
      console.log(action.payload.value[0]);
      if (currentState.length < 1) {
        currentState.value.push(action.payload.value[0]);
        return;
      }

      return {
        value: [...currentState.value, action.payload.value[0]],
      };
    },
    deleteProduct: (state: any, action: PayloadAction<string>) => {
      const currentState = current(state);
      const newProducts = currentState.value.filter((product: any) => product.id !== action.payload);
      return { value: newProducts };
    },
  },
});

export const { addProduct, deleteProduct } = productSlice.actions;

export default productSlice.reducer;
