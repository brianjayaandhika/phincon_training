"use client";

import { useState } from "react";
import { useAppDispatch } from "../redux/hooks";
import { addProduct } from "@/redux/features/productSlices";
import { v4 as uuidv4 } from "uuid";

import Image from "next/image";
import cartIconWhite from "../public/icon-cart-white.svg";
import minusIcon from "../public/icon-minus.svg";
import previousArrow from "../public/icon-previous.svg";
import nextArrow from "../public/icon-next.svg";
import plusIcon from "../public/icon-plus.svg";
import closeIcon from "../public/icon-close.svg";
import thumbnailOne from "../public/image-product-1-thumbnail.jpg";
import thumbnailTwo from "../public/image-product-2-thumbnail.jpg";
import thumbnailThree from "../public/image-product-3-thumbnail.jpg";
import thumbnailFour from "../public/image-product-4-thumbnail.jpg";
import productOne from "../public/image-product-1.jpg";
import productTwo from "../public/image-product-2.jpg";
import productThree from "../public/image-product-3.jpg";
import productFour from "../public/image-product-4.jpg";

const Home = () => {
  const products = [productOne, productTwo, productThree, productFour];

  const [productIndex, setProductIndex] = useState<number>(0);
  const [counter, setCounter] = useState<number>(0);
  const [openModal, setOpenModal] = useState<boolean>(false);

  // Hooks
  const dispatch = useAppDispatch();

  // Handle change display product
  const displayProduct = (index: number): void => {
    setProductIndex(index);
  };

  // Handle counter
  const handleCounter = (action: string): void => {
    if (action === "plus") {
      setCounter(counter + 1);
    } else {
      if (counter < 1) return;
      setCounter(counter - 1);
    }
  };

  // Handle add to cart (add to redux with dispatch)
  const handleAddToCart = (counter: number): void => {
    if (counter === 0) return;

    const dataToAdd = {
      value: [
        {
          id: uuidv4(),
          name: "Fall Limited Sneakers",
          singlePrice: 125,
          amount: counter,
          image: products[productIndex],
        },
      ],
    };
    dispatch(addProduct(dataToAdd));
    setCounter(0);
  };

  // Handle Modal
  const handleOpenModal = () => {
    setOpenModal(!openModal);
  };

  return (
    <main className="flex flex-col md:flex-row justify-between pt-0 pb-4 md:pb-0 md:pt-8">
      {/* next - Image Gallery */}
      <div className="w-full md:w-2/3 flex flex-col gap-8 justify-center items-center md:px-8 lg:px-0 px-0 relative">
        <Image onClick={() => handleOpenModal()} src={products[productIndex]} alt="thumbnail" className="md:rounded-lg md:w-full h-72 md:object-fill object-cover md:cursor-pointer md:hover:opacity-75" />

        <button
          className="absolute top-1/2 left-14 -translate-x-12 -translate-y-6 z-20 hover:opacity-100 bg-white rounded-full px-3 py-2 flex items-center justify-center md:hidden cursor-pointer opacity-50"
          onClick={() => {
            if (productIndex > 0) {
              setProductIndex(productIndex - 1);
            }
          }}
        >
          <Image src={previousArrow} alt="previous" />
        </button>
        <button
          className="absolute top-1/2 right-14 translate-x-12 -translate-y-6 z-20 hover:opacity-100 bg-white rounded-full px-3 py-2 flex items-center justify-center md:hidden cursor-pointer opacity-50"
          onClick={() => {
            if (productIndex < 3) {
              setProductIndex(productIndex + 1);
            }
          }}
        >
          <Image src={nextArrow} alt="next" />
        </button>

        {/* Image Group */}
        <div className="items-center justify-between cursor-pointer w-full hidden md:flex">
          <Image onClick={(): void => displayProduct(0)} src={thumbnailOne} alt="thumbnail" className={` rounded-lg hover:opacity-75 ${productIndex === 0 ? "border-4 border-orange-400 " : ""} w-1/5`} />
          <Image onClick={(): void => displayProduct(1)} src={thumbnailTwo} alt="thumbnail" className={` rounded-lg hover:opacity-75 ${productIndex === 1 ? "border-4 border-orange-400 " : ""} w-1/5`} />
          <Image onClick={(): void => displayProduct(2)} src={thumbnailThree} alt="thumbnail" className={` rounded-lg hover:opacity-75 ${productIndex === 2 ? "border-4 border-orange-400 " : ""} w-1/5`} />
          <Image onClick={(): void => displayProduct(3)} src={thumbnailFour} alt="thumbnail" className={` rounded-lg hover:opacity-75 ${productIndex === 3 ? "border-4 border-orange-400 " : ""} w-1/5`} />
        </div>
      </div>
      {/* Right - Description*/}
      <div className="w-full md-w-1/3 text-black pt-6 md:pt-12 flex flex-col gap-2 md:gap-6 px-8 md:ms-12">
        <p className="text-orange-500 text-sm md:text-xs uppercase font-bold tracking-widest">sneaker company</p>
        <h1 className="text-black text-xl md:text-4xl capitalize font-bold">Fall Limited Edition Sneakers</h1>
        <p className="text-black text-opacity-60  md:pr-0 w-full md:w-2/3 text-md">
          These low-profile sneakers are your perfect casual wear companion. Featuring a durable rubber outer sole, they`ll withstand everything the weather can offer.
        </p>
        <div className="flex items-center gap-4 w-full group-last:text-3xl justify-between md:justify-start">
          <div className="flex gap-4">
            <p className="text-black text-2xl font-bold ">$125.00</p>
            <p className="text-orange-400 bg-pale-orange rounded-md py-1 px-2 text-sm">50%</p>
          </div>
          <p className="text-black text-opacity-60 line-through">$250.00</p>
        </div>
        <div className="flex justify-start gap-4 md:gap-6 w-full flex-col">
          <div className="bg-light-grayish-blue w-full md:w-1/2 flex items-center justify-between gap-12 md:gap-6 py-4 px-3 md:px-7 rounded-md ">
            <button className="cursor-pointer px-4 md:px-0" onClick={() => handleCounter("minus")}>
              <Image src={minusIcon} alt="cart icon" className="w-3 text-orange-400" />
            </button>
            <p className="text-black font-bold px-4">{counter}</p>
            <button className="cursor-pointer px-4 md:px-0" onClick={() => handleCounter("plus")}>
              <Image src={plusIcon} alt="cart icon" className="w-3 h-3 text-orange-400" />
            </button>
          </div>
          <button
            className="text-white bg-orange-500 md:w-1/2 w-full flex items-center gap-4 justify-center py-4 md:py-2  rounded-xl text-sm hover:opacity-40 hover:shadow-xl hover:shadow-orange-200"
            onClick={() => handleAddToCart(counter)}
          >
            <Image src={cartIconWhite} alt="cart icon" className="w-4 h-4 text-white" />
            Add to cart
          </button>
        </div>
      </div>

      {/* Modal */}
      {openModal && (
        <div className="fixed hidden md:block top-0 left-0 w-full h-screen bg-black-opacity-75">
          <div className="fixed top-1/2 left-1/2 -translate-x-1/2 -translate-y-2/3 z-50 p-4 w-96 h-96 flex flex-col gap-6">
            <button onClick={handleOpenModal} className="flex items-center absolute top-0 right-0 -mt-1 bg-white rounded-full p-2 hover:opacity-75">
              <Image src={closeIcon} alt="close" width={24} />
            </button>
            <Image src={products[productIndex]} alt="thumbnail" width={450} height={450} className="md:rounded-lg md:object-fill object-cover" />
            {/* Image Group */}
            <div className="items-center justify-between cursor-pointer w-full hidden md:flex">
              <Image onClick={(): void => displayProduct(0)} src={thumbnailOne} alt="thumbnail" className={` rounded-lg hover:opacity-75 ${productIndex === 0 ? "border-4 border-orange-400 " : ""} w-1/5`} />
              <Image onClick={(): void => displayProduct(1)} src={thumbnailTwo} alt="thumbnail" className={` rounded-lg hover:opacity-75 ${productIndex === 1 ? "border-4 border-orange-400 " : ""} w-1/5`} />
              <Image onClick={(): void => displayProduct(2)} src={thumbnailThree} alt="thumbnail" className={` rounded-lg hover:opacity-75 ${productIndex === 2 ? "border-4 border-orange-400 " : ""} w-1/5`} />
              <Image onClick={(): void => displayProduct(3)} src={thumbnailFour} alt="thumbnail" className={` rounded-lg hover:opacity-75 ${productIndex === 3 ? "border-4 border-orange-400 " : ""} w-1/5`} />
            </div>
          </div>
          <button
            className={`absolute top-1/3 left-1/4 z-50 hover:opacity-100 bg-white rounded-full px-3 py-2 flex items-center justify-center cursor-pointer opacity-50`}
            onClick={() => {
              if (productIndex > 0) {
                setProductIndex(productIndex - 1);
              }
            }}
          >
            <Image src={previousArrow} alt="previous" />
          </button>
          <button
            className="absolute top-1/3 right-1/4 z-50 hover:opacity-100 bg-white rounded-full px-3 py-2 flex items-center justify-center cursor-pointer opacity-50"
            onClick={() => {
              if (productIndex < 3) {
                setProductIndex(productIndex + 1);
              }
            }}
          >
            <Image src={nextArrow} alt="next" />
          </button>
        </div>
      )}
    </main>
  );
};

export default Home;
