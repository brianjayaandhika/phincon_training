import "./globals.css";
import type { Metadata } from "next";
import Navbar from "@/components/Navbar";
import { Providers } from "../redux/provider";

export const metadata: Metadata = {
  title: "E-Commerce",
  description: "Buy shoes online",
};

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang="en">
      <body className="px-0 lg:px-40">
        <Providers>
          <Navbar />
          {children}
        </Providers>
      </body>
    </html>
  );
}
