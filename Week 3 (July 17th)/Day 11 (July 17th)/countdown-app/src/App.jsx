import "./App.css";
import hillPattern from "../src/assets/images/pattern-hills.svg";
import { FaFacebook, FaInstagram, FaPinterest } from "react-icons/fa";
import { useState, useEffect } from "react";

function App() {
  const [days, setDays] = useState("...");
  const [hours, setHours] = useState("...");
  const [minutes, setMinutes] = useState("...");
  const [seconds, setSeconds] = useState("...");
  const [loading, setLoading] = useState(false);

  // COUNTDOWN HANDLE
  const url = new URL(window.location.href);
  const date = url.searchParams.get("date");
  const targetDate = "Jul 17, 2024 12:00:00";
  let countDownDate = date ? new Date(date).getTime() : new Date(targetDate).getTime();

  const timer = () => {
    let interval = setInterval(function () {
      let now = new Date().getTime();
      let countdown = countDownDate - now;

      setDays(Math.floor(countdown / (1000 * 60 * 60 * 24)));
      setHours(Math.floor((countdown % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)));
      setMinutes(Math.floor((countdown % (1000 * 60 * 60)) / (1000 * 60)));
      setSeconds(Math.floor((countdown % (1000 * 60)) / 1000));

      // Display the message when countdown is over
      if (countdown < 0) {
        clearInterval(interval);
      }
    }, 1000);
  };

  // UNAVAILABLE FEATURE HANDLE
  const notReady = (e) => {
    e.preventDefault();
    alert("Sorry, feature is currently unavailable!");
  };

  useEffect(() => {
    if (!loading) {
      timer();
      setLoading(true);
    }
  }, []);

  useEffect(() => {
    document.querySelector(".days").classList.toggle("flip");
    setTimeout(() => {
      document.querySelector(".days").classList.remove("flip");
    }, 820);
  }, [days]);

  useEffect(() => {
    document.querySelector(".hours").classList.toggle("flip");
    setTimeout(() => {
      document.querySelector(".hours").classList.remove("flip");
    }, 820);
  }, [hours]);

  useEffect(() => {
    document.querySelector(".minutes").classList.toggle("flip");
    setTimeout(() => {
      document.querySelector(".minutes").classList.remove("flip");
    }, 820);
  }, [minutes]);

  useEffect(() => {
    document.querySelector(".seconds").classList.toggle("flip");

    setTimeout(() => {
      document.querySelector(".seconds").classList.remove("flip");
    }, 820);
  }, [seconds]);

  return (
    <>
      <div className="app-container">
        <h1 className="app-title">WE'RE LAUNCHING SOON</h1>
        <div className="countdown-section">
          <figure className="box-parent">
            <div className="box">
              <p className="box-time days">{days}</p>
              <div className="cover-box"></div>
              <div className="top-box"></div>

              <div className="bottom-box"></div>
            </div>
            <figcaption>
              <p className="countdown-label-text">DAYS</p>
            </figcaption>
          </figure>
          <figure className="box-parent">
            <div className="box">
              <p className="box-time hours">{hours}</p>
              <div className="cover-box"></div>
              <div className="top-box"></div>
              <div className="bottom-box"></div>
            </div>
            <figcaption>
              <p className="countdown-label-text">HOURS</p>
            </figcaption>
          </figure>
          <figure className="box-parent">
            <div className="box">
              <p className="box-time minutes">{minutes}</p>
              <div className="cover-box"></div>
              <div className="top-box"></div>

              <div className="bottom-box"></div>
            </div>
            <figcaption>
              <p className="countdown-label-text">MINUTES</p>
            </figcaption>
          </figure>
          <figure className="box-parent">
            <div className="box">
              <p className="box-time seconds">{seconds}</p>
              <div className="cover-box"></div>
              <div className="top-box"></div>

              <div className="bottom-box"></div>
            </div>
            <figcaption>
              <p className="countdown-label-text">SECONDS</p>
            </figcaption>
          </figure>
        </div>

        <p className="until-text">
          Until <br />
          {new Date(countDownDate).toString().slice(0, 24)}
        </p>

        <div className="logo-area">
          <a href="/" className="logo-area-btn" onClick={notReady}>
            <FaFacebook />
          </a>
          <a href="/" className="logo-area-btn" onClick={notReady}>
            <FaInstagram />
          </a>
          <a href="/" className="logo-area-btn" onClick={notReady}>
            <FaPinterest />
          </a>
        </div>

        <img src={hillPattern} className="pattern-img" alt="hill-pattern" />
      </div>
    </>
  );
}

export default App;
