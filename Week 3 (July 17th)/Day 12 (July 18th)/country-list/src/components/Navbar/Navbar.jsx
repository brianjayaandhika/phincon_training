import React from "react";
import DarkModeIcon from "@mui/icons-material/DarkMode";
import LightModeIcon from "@mui/icons-material/LightMode";
import classes from "./navbar.module.scss";
import { Button } from "@mui/material";

const Navbar = ({ darkMode, handleDarkMode }) => {
  const containerStyle = {
    backgroundColor: darkMode ? "hsl(209, 23%, 22%)" : "white",
    color: darkMode ? "white" : "hsl(200, 15%, 8%)",
  };

  return (
    <>
      <nav
        style={containerStyle}
        className={`${classes.navbarContainer} ${
          !darkMode ? `classes.light` : null
        }`}
      >
        <h1 className={classes.navbarTitle}>Where in the world?</h1>
        <Button
          style={{
            backgroundColor: darkMode ? "hsl(209, 23%, 22%)" : "white",
            color: darkMode ? "white" : "hsl(209, 23%, 22%)",
          }}
          onClick={() => handleDarkMode()}
          className={classes.btnDarkMode}
        >
          {!darkMode ? (
            <DarkModeIcon className={classes.icon} />
          ) : (
            <LightModeIcon className={classes.icon} />
          )}
        </Button>
      </nav>
    </>
  );
};

export default Navbar;
