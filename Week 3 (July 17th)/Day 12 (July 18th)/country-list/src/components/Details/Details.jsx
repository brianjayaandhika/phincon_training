import classes from "./details.module.scss";
import { useSearchParams } from "react-router-dom";
import KeyboardBackspaceIcon from "@mui/icons-material/KeyboardBackspace";
import Navbar from "../Navbar/Navbar";
import { useState, useEffect } from "react";
import { useMediaQuery } from "@mui/material";
import { Button, styled, Typography, Box, Skeleton } from "@mui/material";
import axios from "axios";

const Details = () => {
  const [searchParams] = useSearchParams();
  const name = searchParams.get("name");

  const [details, setDetails] = useState([]);
  const [loading, setLoading] = useState(true);

  const getDetails = () => {
    axios({
      method: "get",
      url: `https://restcountries.com/v3.1/name/${name}?fullText=true`,
    })
      .then((response) => {
        setDetails(response.data);
        setLoading(false);
      })
      .catch(() => {
        alert("Sorry, something went wrong!");
        window.location.assign("/");
      });
  };

  // Dark mode
  const storedDarkMode = JSON.parse(localStorage.getItem("darkMode"));
  const prefersDarkMode = useMediaQuery("(prefers-color-scheme: dark)");
  const [darkMode, setDarkMode] = useState(
    storedDarkMode !== null ? storedDarkMode : prefersDarkMode
  );

  useEffect(() => {
    localStorage.setItem("darkMode", JSON.stringify(darkMode));
  }, [darkMode]);

  const handleDarkMode = () => {
    setDarkMode(!darkMode);
  };

  const ThemeButton = styled(Button)(() => ({
    width: "fit-content",
    color: darkMode ? "white" : "hsl(200, 15%, 8%)",
    borderColor: darkMode ? "white" : "hsl(200, 15%, 8%)",
    padding: "0.5rem 2rem",
    backgroundColor: darkMode ? "hsl(209, 23%, 22%)" : "white",
    textTransform: "capitalize",
    "&:hover": {
      backgroundColor: darkMode ? "#475569" : "#e2e8f0",
    },
  }));

  // Back to home handle
  const handleHome = () => {
    window.location.assign("/");
  };

  const themeStyled = {
    backgroundColor: darkMode ? "hsl(207, 26%, 17%)" : "hsl(0, 0%, 98%)",
    color: darkMode ? "white" : "hsl(200, 15%, 8%)",
  };

  const textStyled = {
    backgroundColor: darkMode ? "hsl(207, 26%, 17%)" : "hsl(0, 0%, 98%)",
    color: darkMode ? "white" : "hsl(200, 15%, 8%)",
    lineHeight: "32px",
  };

  // Loading Style
  const loadingPage = {
    height: loading ? "100vh" : "100vh",
    backgroundColor: loading
      ? darkMode
        ? "hsl(207, 26%, 17%)"
        : "hsl(0, 0%, 98%)"
      : null,
  };

  useEffect(() => {
    getDetails();
  }, []);

  // Komponen

  // Border Country
  const BorderCountryButtons = ({ borders, classes }) => (
    <span className={classes.buttonBordersArea}>
      {borders && borders.length > 0 ? (
        borders.map((border, index) => (
          <span key={index}>
            <Button variant="outlined" className={classes.buttonBorders}>
              {border}
            </Button>
          </span>
        ))
      ) : (
        <span> None</span>
      )}
    </span>
  );

  // Detail (all)
  const CountryDetail = ({ country, classes }) => (
    <div className={classes.detailCountry}>
      <img src={country.flags.svg} alt="flag" className={classes.detailImg} />
      <div className={classes.detailDesc}>
        <Typography
          style={textStyled}
          className={classes.detailTitle}
          htmlFor="countryName"
          variant="h5"
        >
          {country.name.common ? country.name.common : "None"}
        </Typography>

        <div className={classes.detailSubtitle}>
          <div className={classes.detailSubtitleLeft}>
            <Typography style={textStyled} htmlFor="subtitles" variant="body2">
              <b className={classes.detailSub}>Native Name: </b>{" "}
              <span className={classes.detailSubtitlesSpan}>
                {country.name.nativeName
                  ? Object.values(country.name.nativeName)[0].common
                  : "None"}
              </span>
            </Typography>

            <Typography style={textStyled} htmlFor="subtitles" variant="body2">
              <b className={classes.detailSub}>Population: </b>
              <span className={classes.detailSubtitlesSpan}>
                {country.population
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
              </span>
            </Typography>

            <Typography style={textStyled} htmlFor="subtitles" variant="body2">
              <b className={classes.detailSub}>Region: </b>
              <span className={classes.detailSubtitlesSpan}>
                {country.region ? country.region : "None"}
              </span>
            </Typography>

            <Typography style={textStyled} htmlFor="subtitles" variant="body2">
              <b className={classes.detailSub}>Sub Region: </b>
              <span className={classes.detailSubtitlesSpan}>
                {country.subregion ? country.subregion : "None"}
              </span>
            </Typography>

            <Typography style={textStyled} htmlFor="subtitles" variant="body2">
              <b className={classes.detailSub}>Capital: </b>
              <span className={classes.detailSubtitlesSpan}>
                {country.capital ? country.capital : "None"}
              </span>
            </Typography>
          </div>

          <div className={classes.detailSubtitleRight}>
            <Typography style={textStyled} htmlFor="subtitles" variant="body2">
              <b className={classes.detailSub}>Top Level Domain: </b>
              <span className={classes.detailSubtitlesSpan}>
                {country.tld ? country.tld : "None"}
              </span>
            </Typography>

            <Typography style={textStyled} htmlFor="subtitles" variant="body2">
              <b className={classes.detailSub}>Currencies: </b>
              <span className={classes.detailSubtitlesSpan}>
                {country.currencies
                  ? Object.values(country.currencies)
                      .map((currency) => currency.name)
                      .join(", ")
                  : "None"}
              </span>
            </Typography>

            <Typography style={textStyled} htmlFor="subtitles" variant="body2">
              <b className={classes.detailSub}>Languages: </b>
              <span className={classes.detailSubtitlesSpan}>
                {country.languages
                  ? Object.values(country.languages).map(
                      (language, i, languagesArray) => (
                        <span key={i}>
                          {language}
                          {i < languagesArray.length - 1 && ", "}
                        </span>
                      )
                    )
                  : "None"}
              </span>
            </Typography>
          </div>
        </div>

        <Typography style={textStyled} htmlFor="Border Countries">
          <b className={classes.detailSub}>Border Countries:</b>
          <BorderCountryButtons borders={country.borders} classes={classes} />
        </Typography>
      </div>
    </div>
  );

  return (
    <>
      <Navbar darkMode={darkMode} handleDarkMode={handleDarkMode} />
      <div className={classes.detailContainer} style={themeStyled}>
        <ThemeButton
          onClick={() => handleHome()}
          variant="outlined"
          startIcon={<KeyboardBackspaceIcon />}
        >
          Back
        </ThemeButton>

        {/* Detail negara */}
        {loading ? (
          <Box className={classes.skeletonWrapper}>
            {/* skeleton img */}
            <Skeleton
              animation="wave"
              variant="rectangular"
              className={classes.skeletonImg}
            />
            {/* div utk skeleton texts */}
            <Box className={classes.skeletonDesc}>
              {/* skeleton utk title */}
              <Skeleton
                animation="wave"
                variant="text"
                className={classes.skeletonTitle}
                sx={{ marginBottom: "24px" }}
              />
              {/* akan ada jarak (pake margin aja) */}
              {/* skeleton buat text ada 5 biji */}
              <Skeleton
                animation="wave"
                variant="text"
                className={classes.skeletonText}
              />
              <Skeleton
                animation="wave"
                variant="text"
                className={classes.skeletonText}
              />
              <Skeleton
                animation="wave"
                variant="text"
                className={classes.skeletonText}
              />
              <Skeleton
                animation="wave"
                variant="text"
                className={classes.skeletonText}
              />
              <Skeleton
                animation="wave"
                variant="text"
                className={classes.skeletonText}
              />
              {/* skeleton utk border countri */}
              <Skeleton
                animation="wave"
                variant="rectangle"
                className={classes.skeletonBorder}
              />
            </Box>
          </Box>
        ) : (
          details?.map((country, i) => (
            <CountryDetail key={i} country={country} classes={classes} />
          ))
        )}
      </div>
    </>
  );
};

export default Details;
