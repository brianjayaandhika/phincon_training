import classes from "./content.module.scss";

import { useEffect, useState } from "react";
import {
  MenuItem,
  InputLabel,
  Select,
  FormControl,
  TextField,
  IconButton,
  Skeleton,
  Box,
} from "@mui/material";
import axios from "axios";
import Cards from "../Cards/Cards";
import SortRoundedIcon from "@mui/icons-material/SortRounded";

const Content = ({ darkMode }) => {
  const [region, setRegion] = useState("All");
  const [filtered, setFiltered] = useState(false);
  const [searched, setSearched] = useState(false);
  const [loading, setLoading] = useState(true);
  const [sort, setSort] = useState(true);

  const [countryList, setCountryList] = useState([]);
  const [filteredList, setFilteredList] = useState([]);
  const [searchedList, setSearchedList] = useState([]);

  // Get all country list
  const getCountryList = () => {
    axios({
      method: "get",
      url: "https://restcountries.com/v3.1/all",
    })
      .then((response) => {
        setCountryList(
          sort
            ? response.data.sort((a, b) =>
                a.name.common.localeCompare(b.name.common)
              )
            : response.data.sort((a, b) =>
                b.name.common.localeCompare(a.name.common)
              )
        );
        setLoading(false);
      })

      .catch((err) => {
        console.error(err);
      });
  };

  // Filter by region handle
  const handleChange = (event) => {
    setRegion(event.target.value);

    setFiltered(true);
    setFilteredList(
      countryList.filter((country) => country.region === event.target.value)
    );

    if (event.target.value === "All") {
      setFiltered(false);
      setFilteredList([]);
    }
  };

  // Filter by search
  const handleSearch = (event) => {
    setSearchedList(
      countryList.filter((country) =>
        country.name.common
          .toLowerCase()
          .includes(event.target.value.toLowerCase())
      )
    );

    if (event.target.value !== "") {
      setSearched(true);
    } else {
      setSearched(false);
    }
  };

  // Sorting handle
  const handleSort = () => {
    setSort((prevState) => !prevState);
  };
  // Style

  // Theme Style
  const themeStyled = {
    backgroundColor: darkMode ? "hsl(207, 26%, 17%)" : "hsl(0, 0%, 98%)",
    color: darkMode ? "white" : "hsl(2  00, 15%, 8%)",
  };

  useEffect(() => {
    getCountryList();
  }, [sort]);

  return (
    <>
      <div className={classes.contentContainer} style={themeStyled}>
        <div className={classes.inputArea}>
          {/* Search Bar */}
          <TextField
            id="outlined-multiline-flexible"
            label="Search for a country..."
            maxRows={4}
            className={classes.searchInput}
            onChange={handleSearch}
            disabled={filtered}
          />
          <div className={classes.rightInputArea}>
            {/* Dropdown Bar */}
            <div className={classes.dropdownBar}>
              <FormControl fullWidth sx={{ m: 1 }} disabled={searched}>
                <InputLabel id="demo-simple-select-label">Region</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={region}
                  label="region"
                  onChange={handleChange}
                >
                  <MenuItem value={"All"}>All</MenuItem>
                  <MenuItem value={"Africa"}>Africa</MenuItem>
                  <MenuItem value={"Americas"}>America</MenuItem>
                  <MenuItem value={"Asia"}>Asia</MenuItem>
                  <MenuItem value={"Europe"}>Europe</MenuItem>
                  <MenuItem value={"Oceania"}>Oceania</MenuItem>
                </Select>
              </FormControl>
            </div>
            {/* Sort Button */}
            <IconButton onClick={() => handleSort()} className={classes.btn}>
              <SortRoundedIcon
                color="inherit"
                style={sort ? { transform: "rotateX(180deg)" } : null}
              />
            </IconButton>
          </div>
        </div>

        {loading ? (
          <div className={classes.cardArea}>
            {Array.from({ length: 8 }).map((_, index) => (
              <Box key={index} className={classes.skeletonBox}>
                <Skeleton
                  animation="wave"
                  variant="rectangular"
                  className={classes.skeleton}
                />
              </Box>
            ))}
          </div>
        ) : (
          <div className={classes.cardArea}>
            {filtered
              ? filteredList?.map((country, i) => (
                  <Cards
                    darkMode={darkMode}
                    key={i}
                    country={country}
                    classes={classes}
                  />
                ))
              : searched
              ? searchedList?.map((country, i) => (
                  <Cards
                    darkMode={darkMode}
                    key={i}
                    country={country}
                    classes={classes}
                  />
                ))
              : countryList?.map((country, i) => (
                  <Cards
                    darkMode={darkMode}
                    key={i}
                    country={country}
                    classes={classes}
                  />
                ))}
          </div>
        )}
      </div>
    </>
  );
};

export default Content;
