import React from "react";
import Card from "@mui/material/Card";
import CardActionArea from "@mui/material/CardActionArea";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

const Cards = ({ country, classes, darkMode }) => {
  const themeStyled = {
    backgroundColor: darkMode ? "hsl(207, 26%, 17%)" : "white",
    color: darkMode ? "white" : "hsl(200, 15%, 8%)",
  };

  // Get country details
  const getCountryDetails = () => {
    window.location.assign(`/details?name=${country.name.common}`);
  };

  return (
    <Card className={classes.cards} style={themeStyled} onClick={() => getCountryDetails()}>
      <CardActionArea>
        <CardMedia component="img" className={classes.cardImage} image={country.flags.svg} alt="country flag" />
        <CardContent sx={{ minHeight: 170 }}>
          <Typography variant="h6" sx={{ fontWeight: "bold" }} gutterBottom component="div">
            {country.name.common}
          </Typography>
          <Typography variant="body2" gutterBottom component="div">
            Population: {country.population.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
          </Typography>
          <Typography variant="body2" gutterBottom component="div">
            Region: {country.region}
          </Typography>
          <Typography variant="body2" gutterBottom component="div">
            Capital: {country.capital}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export default Cards;
