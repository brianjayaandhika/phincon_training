import "./base.module.scss";
import Navbar from "../src/components/Navbar/Navbar";
import Content from "../src/components/Content/Content";
import { useState, useMemo, useEffect } from "react";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import { useMediaQuery, responsiveFontSizes } from "@mui/material";

function App() {
  // Dark mode
  const storedDarkMode = JSON.parse(localStorage.getItem("darkMode"));
  const prefersDarkMode = useMediaQuery("(prefers-color-scheme: dark)");
  const [darkMode, setDarkMode] = useState(storedDarkMode !== null ? storedDarkMode : prefersDarkMode);

  // Theme
  const theme = useMemo(() => {
    const mainPalette = {
      primary: {
        main: "#6366f1",
      },
      background: {
        default: "#f1f5f9",
      },
    };

    const darkModePalette = {
      primary: {
        main: "#4f46e5",
      },
      background: {
        default: "#1e293b",
      },
    };

    return createTheme({
      palette: {
        mode: darkMode ? "dark" : "light",
        ...mainPalette,
        ...(darkMode ? darkModePalette : {}),
      },
    });
  }, [darkMode]);

  const themeStyled = {
    backgroundColor: darkMode ? "hsl(207, 26%, 17%)" : "hsl(0, 0%, 98%)",
    color: darkMode ? "white" : "hsl(2  00, 15%, 8%)",
  };

  // Save dark mode to local storage
  useEffect(() => {
    localStorage.setItem("darkMode", JSON.stringify(darkMode));
  }, [darkMode]);

  // Handle dark mode
  const handleDarkMode = () => {
    setDarkMode(!darkMode);
  };

  // Responsive fonts
  const themeWithResponsiveFonts = responsiveFontSizes(theme);

  return (
    <>
      <ThemeProvider theme={themeWithResponsiveFonts}>
        <Navbar darkMode={darkMode} handleDarkMode={handleDarkMode} />
        <Content darkMode={darkMode} />
      </ThemeProvider>
    </>
  );
}

export default App;
