export const SET_LOCAL = 'App/SET_LOCAL';
export const SET_THEME = 'App/SET_THEME';

export const GET_ADVICE = 'App/GET_ADVICE';
export const SET_ADVICE = 'App/SET_ADVICE';
export const SET_LOADING = 'App/SET_LOADING';
