import { call, put, takeLatest } from 'redux-saga/effects';
import { GET_ADVICE } from './constants';
import { getAdvice } from '@domain/api';
import { setAdvice, setLoading } from './actions';

export function* doGetRandomAdvice() {
  yield put(setLoading(true));

  try {
    const response = yield call(getAdvice);
    yield put(setAdvice(response.slip));
  } catch (err) {
    console.error(err);
  }

  yield put(setLoading(false));
}

export default function* appSaga() {
  yield takeLatest(GET_ADVICE, doGetRandomAdvice);
}
