import classes from './style.module.scss';
import { PropTypes } from 'prop-types';
import { useEffect, useState } from 'react';
import { TypeAnimation } from 'react-type-animation';

// Material UI
import CasinoIcon from '@mui/icons-material/Casino';
import LightModeIcon from '@mui/icons-material/LightMode';
import NightsStayIcon from '@mui/icons-material/NightsStay';
import AcUnitIcon from '@mui/icons-material/AcUnit';
import LocalFireDepartmentIcon from '@mui/icons-material/LocalFireDepartment';
import { Box, CircularProgress } from '@mui/material';

// Redux
import { useDispatch } from 'react-redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { selectTheme, selectAdvice, selectLoading } from '@containers/App/selectors';
import { getAdvice, setTheme } from '@containers/App/actions';

const Home = ({ theme, advice, loading }) => {
  const dispatch = useDispatch();
  const [cooldown, setCooldown] = useState(false);

  // theme handle
  const handleTheme = (event) => {
    const name = event.currentTarget.getAttribute('name');

    dispatch(setTheme((theme = name)));
  };

  // recall API
  const handleGetAdvice = () => {
    !cooldown && dispatch(getAdvice());
    setCooldown(true);

    setTimeout(() => {
      setCooldown(false);
    }, 3000);
  };

  useEffect(() => {
    dispatch(getAdvice());
  }, [dispatch]);

  return (
    <div className={classes.wrapper}>
      <div className={classes.box}>
        <div className={classes.btnGroup}>
          <button className={classes.themeBtn} id={classes.light} name="light" onClick={handleTheme}>
            <LightModeIcon />
          </button>
          <button className={classes.themeBtn} id={classes.dark} name="dark" onClick={handleTheme}>
            <NightsStayIcon name="dark" />
          </button>
          <button className={classes.themeBtn} id={classes.cold} name="cold" onClick={handleTheme}>
            <AcUnitIcon name="cold" />
          </button>
          <button className={classes.themeBtn} id={classes.warm} name="warm" onClick={handleTheme}>
            <LocalFireDepartmentIcon name="warm" />
          </button>
        </div>

        {loading ? (
          <Box className={classes.skeletonBox}>
            <CircularProgress className={classes.load} />
          </Box>
        ) : (
          <div className={classes.textBox}>
            <p className={classes.title}>ADVICE #{advice.id}</p>
            <TypeAnimation sequence={[`${advice.advice}`]} wrapper="span" speed={150} className={classes.advice} />
            {/* <p></p> */}
          </div>
        )}

        <div className={classes.line}>||</div>
      </div>
      <button disabled={cooldown} className={classes.button} onClick={handleGetAdvice}>
        <CasinoIcon className={classes.dice} />
      </button>
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  theme: selectTheme,
  advice: selectAdvice,
  loading: selectLoading,
});

Home.propTypes = {
  theme: PropTypes.string,
  advice: PropTypes.object,
  loading: PropTypes.bool,
};

export default connect(mapStateToProps)(Home);
