import MainLayout from '@layouts/MainLayout';

import Todo from '@pages/Todo';
import NotFound from '@pages/NotFound';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Todo,
  },

  { path: '*', name: 'Not Found', component: NotFound, layout: MainLayout },
];

export default routes;
