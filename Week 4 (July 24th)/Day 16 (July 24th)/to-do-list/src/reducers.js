import { combineReducers } from 'redux';

import appReducer from '@containers/App/reducer';
import languageReducer, { storedKey as storedLangState } from '@containers/Language/reducer';
import tasksReducer, { storedKey as storedTasksState } from '@containers/Tasks/reducer';
import themeReducer, { storedKey as storedThemeState } from '@containers/Theme/reducer';

import { mapWithPersistor } from './persistence';

const storedReducers = {
  language: { reducer: languageReducer, whitelist: storedLangState },
  tasks: { reducer: tasksReducer, whitelist: storedTasksState },
  theme: { reducer: themeReducer, whitelist: storedThemeState },
};

const temporaryReducers = {
  app: appReducer,
};

const createReducer = () => {
  const coreReducer = combineReducers({
    ...mapWithPersistor(storedReducers),
    ...temporaryReducers,
  });
  const rootReducer = (state, action) => coreReducer(state, action);
  return rootReducer;
};

export default createReducer;
