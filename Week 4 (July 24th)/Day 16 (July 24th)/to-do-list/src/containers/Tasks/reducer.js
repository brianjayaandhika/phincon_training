import { SET_TASKS } from './constants';
import { produce } from 'immer';

export const initialState = {
  tasks: [],
};

export const storedKey = ['tasks'];

// eslint-disable-next-line default-param-last
const tasksReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SET_TASKS:
        draft.tasks = action.payload;
        break;
      default:
        return state;
    }
  });
export default tasksReducer;
