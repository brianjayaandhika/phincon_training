import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectTasksState = (state) => state.tasks || initialState;

const selectTasks = createSelector(selectTasksState, (state) => state.tasks);

export { selectTasks };
