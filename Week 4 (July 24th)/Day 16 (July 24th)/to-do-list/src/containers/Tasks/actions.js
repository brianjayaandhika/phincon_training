import { SET_TASKS } from './constants';

export const setTasks = (tasks) => ({
  type: SET_TASKS,
  payload: tasks,
});
