import { createSelector } from 'reselect';
import { initialState } from '@containers/Theme/reducer';

const selectThemeState = (state) => state.theme || initialState;

const selectTheme = createSelector(selectThemeState, (state) => state.theme);

export { selectTheme };
