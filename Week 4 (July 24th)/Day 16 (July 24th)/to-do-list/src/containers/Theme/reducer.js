import { SET_THEME } from './constants';

const preferredTheme = window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light';

export const initialState = {
  theme: preferredTheme,
};

export const storedKey = ['theme'];

// eslint-disable-next-line default-param-last
const themeReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_THEME:
      return {
        ...state,
        theme: state.theme === 'light' ? 'dark' : 'light',
      };
    default:
      return state;
  }
};

export default themeReducer;
