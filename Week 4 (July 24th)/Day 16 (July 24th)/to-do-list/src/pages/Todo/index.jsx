import React from 'react';
import classes from './styles.module.scss';
import { PropTypes } from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { v4 as uuidv4 } from 'uuid';
import { useState } from 'react';

// import component from MUI
import { Button, TextField, Dialog, DialogActions, DialogContent, DialogTitle, Modal, Box } from '@mui/material';
import { ModalDialog, IconButton, Alert } from '@mui/joy';

// Images and Icons
import moonImg from '@static/images/icon-moon.svg';
import darkBg from '@static/images/bg-desktop-dark.jpg';
import sunImg from '../../static/images/icon-sun.svg';
import lightBg from '@static/images/bg-desktop-light.jpg';
import AddIcon from '@mui/icons-material/Add';
import WarningAmberRoundedIcon from '@mui/icons-material/WarningAmberRounded';
import CloseRoundedIcon from '@mui/icons-material/CloseRounded';

// Redux for theme
import { connect, useDispatch } from 'react-redux';
import { selectTheme } from '@containers/Theme/selectors';
import { setTheme } from '@containers/Theme/actions';

// Redux for tasks
import { selectTasks } from '@containers/Tasks/selectors';
import { setTasks } from '@containers/Tasks/actions';

// Drag and drop from smooth-dnd library
import { Container, Draggable } from 'react-smooth-dnd';
import { applyDrag } from '@utils/smoothDnd';

const Todo = ({ theme, tasks }) => {
  const dispatch = useDispatch();
  const [filter, setFilter] = useState('All');
  const [selectedId, setSelectedId] = useState('');
  const [edittedTask, setEdittedTask] = useState('');
  const [isFilter, setIsFilter] = useState('false');

  // Dialog
  const [openDialog, setOpenDialog] = useState(false);
  const [openModal, setOpenModal] = useState(false);

  // Dialog and Modal Handles
  const handleClickOpenDialog = (taskId) => {
    setOpenDialog(true);
    setSelectedId(taskId);
  };

  const handleClickOpenModal = (taskId) => {
    setOpenModal(true);
    setSelectedId(taskId);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  // Handle Theme
  const isDark = theme === 'dark';
  const onSwitch = () => {
    if (theme === 'dark') {
      dispatch(setTheme('light'));
    } else {
      dispatch(setTheme('dark'));
    }
  };

  // Styles
  const themeStyles = {
    lightModeStyle: {
      Bg: 'hsl(0, 0%, 98%)',
      Bg2: 'hsl(236, 33%, 92%)',
      Text: 'hsl(0, 0%, 100%)',
      Text2: 'hsl(235, 19%, 35%)',
    },
    darkModeStyle: {
      Bg: 'hsl(235, 21%, 11%)',
      Bg2: 'hsl(235, 24%, 19%)',
      Text: 'hsl(0, 0%, 100%)',
      Text2: 'hsl(234, 39%, 85%)',
    },
  };
  const styles = {
    primaryBg: {
      backgroundColor: isDark ? themeStyles.darkModeStyle.Bg : themeStyles.lightModeStyle.Bg,
    },

    secondaryBg: {
      backgroundColor: isDark ? themeStyles.darkModeStyle.Bg2 : themeStyles.lightModeStyle.Bg2,
      color: isDark ? themeStyles.darkModeStyle.Text2 : themeStyles.lightModeStyle.Text2,
    },

    secondaryText: {
      color: isDark ? themeStyles.darkModeStyle.Text2 : themeStyles.lightModeStyle.Text2,
    },

    statusComplete: {
      textDecoration: 'line-through',
      opacity: '0.4',
    },

    activeLink: {
      color: 'hsl(220, 98%, 61%)',
    },

    scrollY: {
      overflowY: 'scroll',
    },

    displayNone: {
      display: 'none',
    },

    draggableStyle: {
      width: '100%',
      borderBottom: '0.2px solid $light-grayish-blue',
      padding: '16px 24px',
      position: 'relative',

      fontSize: '0.75rem',
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      gap: '24px',

      position: 'relative',
    },
  };

  const body = document.querySelector('body');
  body.style.backgroundColor = isDark ? themeStyles.darkModeStyle.Bg : themeStyles.lightModeStyle.Bg;

  // Handles
  // Handle add new task
  const handleAddTask = (event) => {
    event.preventDefault();

    const inputValue = event.target.elements.input.value;
    const trimmedValue = inputValue.trim();

    if (trimmedValue === '') {
      event.target.elements.input.value = '';
      return -1;
    }

    if (inputValue.length > 60) {
      alert('Too many characters');
      event.target.elements.input.value = '';
      return -1;
    }

    const newTask = {
      id: uuidv4(),
      task: inputValue,
      status: 'active',
    };

    dispatch(setTasks([newTask, ...tasks]));

    event.target.elements.input.value = '';
  };
  // Handle delete task
  const handleDeleteTask = () => {
    const updatedTasks = tasks.filter((task) => task.id !== selectedId);
    dispatch(setTasks(updatedTasks));

    setOpenModal(false);
  };
  // Handle filter active/complete
  const handleFilter = (status) => {
    setFilter(`${status.target.innerHTML}`);

    status.target.innerHTML === 'All' ? setIsFilter(false) : setIsFilter(true);
  };
  // Handle checkbox
  const handleCheckboxChange = (taskId) => {
    const updatedTasks = tasks.map((task) => {
      if (task.id === taskId) {
        return {
          ...task,
          status: task.status === 'active' ? 'complete' : 'active',
        };
      }
      return task;
    });

    dispatch(setTasks(updatedTasks));
  };
  // Handle clear complete
  const handleClearComplete = () => {
    const updatedTasks = tasks.filter((task) => task.status !== 'complete');
    dispatch(setTasks(updatedTasks));
  };
  // Handle Edit
  const handleSaveEdit = () => {
    setOpenDialog(false);

    const updatedTasks = tasks.map((task) => {
      if (task.id === selectedId) {
        return {
          ...task,
          task: edittedTask,
        };
      }

      return task;
    });

    dispatch(setTasks(updatedTasks));
  };
  const handleEdit = (event) => {
    setEdittedTask(event.target.value);
  };
  // Drag and Drop handles
  const handleDrop = (e) => {
    dispatch(setTasks(applyDrag(tasks, e, filter)));
  };

  return (
    <div className={classes.appContainer} style={styles.primaryBg}>
      <img src={isDark ? darkBg : lightBg} alt="background image" className={classes.backgroundImage} />
      {/* App layout */}
      <div className={classes.appLayout}>
        {/* Title */}
        <div className={classes.layoutHeader}>
          <h1 className={classes.title}>TODO</h1>
          <Button onClick={onSwitch}>
            <img src={isDark ? moonImg : sunImg} alt="dark mode btn" />
          </Button>
        </div>

        {/* Tasks Layout */}
        <div className={classes.layoutTodo}>
          {/* Input */}
          <form action="/" onSubmit={handleAddTask}>
            <label htmlFor="taskInput" className={classes.inputGroup}>
              <input
                type="text"
                name="input"
                id="taskInput"
                placeholder="Add new task"
                style={styles.secondaryBg}
                className={classes.input}
                required
              />
              <Button type="submit" className={classes.inputBtn} style={styles.secondaryText}>
                <AddIcon />
              </Button>
            </label>
          </form>

          {/* Task Lists */}
          <div
            className={classes.taskGroup}
            style={
              tasks.filter((task) =>
                filter === 'Active'
                  ? task.status === 'active'
                  : filter === 'Completed'
                  ? task.status === 'complete'
                  : true
              ).length > 6
                ? styles.scrollY
                : null
            }
          >
            <Container onDrop={handleDrop} autoScrollEnabled>
              {tasks.filter((task) =>
                filter === 'All' ? task : filter === 'Completed' ? task.status === 'complete' : true
              ).length > 0 ? (
                tasks
                  .filter((task) =>
                    filter === 'Active'
                      ? task.status === 'active'
                      : filter === 'Completed'
                      ? task.status === 'complete'
                      : true
                  )
                  .map((task) => (
                    <Draggable key={task.id}>
                      <div className={classes.taskList} style={styles.secondaryBg}>
                        <label class="form-control">
                          <div className="box">
                            <input
                              type="checkbox"
                              checked={task.status === 'complete'}
                              onChange={() => handleCheckboxChange(task.id)}
                              id={`checkbox ${task.id}`}
                            />
                          </div>
                        </label>
                        <label
                          className={classes.paragraph}
                          style={task.status === 'complete' ? styles.statusComplete : null}
                          htmlFor={`checkbox ${task.id}`}
                        >
                          {task.task}
                        </label>
                        <div className={classes.btnGroup}>
                          <Button
                            className={classes.close}
                            onClick={() => handleClickOpenDialog(task.id)}
                            data-task-id={task.id}
                          >
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="18"
                              height="18"
                              fill="#494C6B"
                              className="bi bi-pencil-square"
                              viewBox="0 0 16 16"
                            >
                              {' '}
                              <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />{' '}
                              <path
                                fillRule="evenodd"
                                d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"
                              />{' '}
                            </svg>
                          </Button>

                          <Button
                            className={classes.close}
                            onClick={() => handleClickOpenModal(task.id)}
                            data-task-id={task.id}
                          >
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18">
                              <path
                                fill="#494C6B"
                                fillRule="evenodd"
                                d="M16.97 0l.708.707L9.546 8.84l8.132 8.132-.707.707-8.132-8.132-8.132 8.132L0 16.97l8.132-8.132L0 .707.707 0 8.84 8.132 16.971 0z"
                              />
                            </svg>
                          </Button>
                        </div>
                      </div>
                    </Draggable>
                  ))
              ) : (
                <div className={classes.taskListEmpty} style={styles.secondaryBg}>
                  No {filter === 'Completed' ? 'completed task' : filter === 'Active' ? 'active task' : 'task'} is
                  available
                </div>
              )}
            </Container>
          </div>

          <div className={classes.layoutTodoFooter} style={styles.secondaryBg}>
            <p className={classes.subtitle} style={styles.secondaryText}>
              {
                tasks.filter((task) =>
                  filter === 'Active'
                    ? task.status === 'active'
                    : filter === 'Completed'
                    ? task.status === 'complete'
                    : true
                ).length
              }{' '}
              task left
            </p>
            {/* Desktop */}
            <div className={classes.functionDesktop} onClick={() => handleFilter(event)}>
              <a
                style={filter === 'All' ? { ...styles.secondaryText, ...styles.activeLink } : styles.secondaryText}
                variant="text"
              >
                All
              </a>
              <a
                style={filter === 'Active' ? { ...styles.secondaryText, ...styles.activeLink } : styles.secondaryText}
                variant="text"
              >
                Active
              </a>
              <a
                style={
                  filter === 'Completed' ? { ...styles.secondaryText, ...styles.activeLink } : styles.secondaryText
                }
                variant="text"
              >
                Completed
              </a>
            </div>
            <a style={styles.secondaryText} className={classes.clearCompleted} onClick={handleClearComplete}>
              Clear Completed
            </a>
          </div>
        </div>

        <div className={classes.bottomLayout}>
          {/* Mobile */}
          <div className={classes.functionMobile} style={styles.secondaryBg} onClick={() => handleFilter(event)}>
            <a
              style={filter === 'All' ? { ...styles.secondaryText, ...styles.activeLink } : styles.secondaryText}
              variant="text"
            >
              All
            </a>
            <a
              style={filter === 'Active' ? { ...styles.secondaryText, ...styles.activeLink } : styles.secondaryText}
              variant="text"
            >
              Active
            </a>
            <a
              style={filter === 'Completed' ? { ...styles.secondaryText, ...styles.activeLink } : styles.secondaryText}
              variant="text"
            >
              Completed
            </a>
          </div>
          <p className={classes.dragText}>Drag and drop to reorder list</p>
        </div>

        {/* Dialog Component */}
        <div htmlFor="dialog-wrapper">
          <Dialog open={openDialog} onClose={handleCloseDialog}>
            <DialogTitle style={styles.secondaryBg}>
              {'edit task id:'}
              <br />
              {selectedId}
            </DialogTitle>
            <DialogContent style={styles.secondaryBg}>
              <TextField
                style={styles.secondaryText}
                autoFocus
                margin="dense"
                className={classes.dialogInput}
                id="name"
                label="Edit task"
                placeholder={`${tasks.filter((task) => task.id === selectedId).map((task) => task.task)}`}
                type="email"
                fullWidth
                variant="standard"
                onChange={() => handleEdit(event)}
                sx={{ marginTop: '24px', input: styles.secondaryText }}
              />
            </DialogContent>
            <DialogActions style={styles.secondaryBg}>
              <Button onClick={handleSaveEdit} color="success" variant="contained">
                Save Changes
              </Button>
              <Button onClick={handleCloseDialog} color="error" variant="contained">
                Cancel
              </Button>
            </DialogActions>
          </Dialog>
        </div>
        {/* Delete Modal */}
        <div htmlFor="deleteModal-wrapper">
          <Modal open={openModal} onClose={() => setOpenModal(false)}>
            <ModalDialog
              variant="outlined"
              role="alertdialog"
              aria-labelledby="alert-dialog-modal-title"
              aria-describedby="alert-dialog-modal-description"
              style={{ ...styles.secondaryBg, ...styles.secondaryText }}
            >
              <p style={{ fontSize: '1rem' }}>
                Confirm Delete Task: <br />{' '}
                {` ${tasks.filter((task) => task.id === selectedId).map((task) => task.task)} ?`}
              </p>

              <hr />

              <Box sx={{ display: 'flex', gap: 2, justifyContent: 'center', p: 2 }}>
                <Button sx={{ fontSize: '0.8rem' }} variant="plain" color="neutral" onClick={() => setOpenModal(false)}>
                  Cancel
                </Button>
                <Button
                  sx={{ fontSize: '0.8rem' }}
                  color="error"
                  variant="contained"
                  onClick={() => handleDeleteTask()}
                >
                  Delete Task
                </Button>
              </Box>
            </ModalDialog>
          </Modal>
        </div>
        {/* Alert */}
        {/* <div htmlFor="alert-wrapper" className={classes.alertBox}>
          <Alert
            variant="soft"
            className={classes.alert}
            color="danger"
            startDecorator={<WarningAmberRoundedIcon />}
            endDecorator={
              <IconButton variant="plain" size="sm" color="neutral">
                <CloseRoundedIcon />
              </IconButton>
            }
          >
            Something went wrong!
          </Alert>
        </div> */}
      </div>
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  theme: selectTheme,
  tasks: selectTasks,
});

Todo.propTypes = {
  theme: PropTypes.string,
  tasks: PropTypes.array,
  dispatch: PropTypes.func,
};

export default connect(mapStateToProps)(Todo);
