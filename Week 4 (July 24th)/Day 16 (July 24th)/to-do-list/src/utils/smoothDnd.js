export const applyDrag = (tasks, dragResult, filter) => {
  const { removedIndex, addedIndex, payload } = dragResult;
  if (removedIndex === null && addedIndex === null) return tasks;

  let result = [...tasks];

  const getRemovedId = (status, index) => {
    const filteredTasks = result.filter((task) => task.status === status);
    return tasks.findIndex((item) => item.id === filteredTasks[index].id);
  };

  let removedId = removedIndex;
  let addedId = addedIndex;

  switch (filter) {
    case 'Active':
      removedId = getRemovedId('active', removedIndex);
      addedId = getRemovedId('active', addedIndex);
      break;
    case 'Completed':
      removedId = getRemovedId('complete', removedIndex);
      addedId = getRemovedId('complete', addedIndex);
      break;
    default:
      break;
  }

  let itemToAdd = payload;

  if (removedId !== null) {
    itemToAdd = result.splice(removedId, 1)[0];
  }

  if (addedId !== null) {
    result.splice(addedId, 0, itemToAdd);
  }

  return result;
};
